package com.droi.sdk.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.droi.sdk.push.utils.Util;

public class TickAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Util.hasNetwork(context) == false) {
            return;
        }

        Intent startSrv = new Intent(Constants.INTENT_PUSHSERVICE);
        startSrv.putExtra(Constants.KEY_CMD, Constants.KEY_TICK);
        startSrv.setPackage(context.getPackageName());
        PushUtil.startPushService(context, startSrv);
    }
}
