package com.droi.sdk.push;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import android.content.Context;
import android.content.Intent;
import com.droi.sdk.push.data.ClientPushableBean;
import com.droi.sdk.push.data.HeartBeatBean;
import com.droi.sdk.push.data.RecoveryTimeBean;
import com.droi.sdk.push.data.ServiceIpsBean;
import com.droi.sdk.push.data.SlienceTimeBean;
import com.droi.sdk.push.data.TagBean;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;

class PushSetting {
    public static final String KEY_SILENT_RECOVERY = "resume";
    static final long EMPTY_RECOVERY_TIME = 0L;
    static final String PUBLIC_KEY = "bc3a2c45";
    static final String DEVICE_APP_KEY = "00000000000000000000000000000000";
    private static PushSetting mInstance;
    private Context mContext;

    static PushSetting getInstance(Context context) {
        synchronized (PushSetting.class) {
            if (mInstance == null) {
                mInstance = new PushSetting(context.getApplicationContext());
            }
            return mInstance;
        }
    }

    private PushSetting(Context context) {
        mContext = context;
    }

    /**
     * Add one tag for the application
     * 
     * @param tag
     *            A string which describes the application
     * @return true if adding is successful, otherwise false
     */
    public boolean addTag(String tag) {
        boolean success = false;
        if (tag != null && tag.length() != 0) {
            success = addTags(new String[] { tag });
        }
        return success;
    }

    /**
     * Add the tags for the application
     * 
     * @param tags
     *            A set of string which describes the application
     * @return true if adding is successful, otherwise false
     */
    public boolean addTags(String[] tags) {
        if (tags == null || tags.length == 0) {
            return false;
        }

        TagBean tBean = null;
        try {
            tBean = (TagBean) Util.getSerializableData(mContext,
                    Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
        } catch (Exception e) {
            tBean = null;
        }

        int curTagsNum = 0;
        Set<String> hSet = new HashSet<String>();
        if (tBean != null && tBean.getTags() != null) {
            curTagsNum = tBean.getTags().length;
            hSet.addAll(Arrays.asList(tBean.getTags()));
        }
        hSet.addAll(Arrays.asList(tags));

        if (curTagsNum != hSet.size()) {
            String[] arr = new String[hSet.size()];
            hSet.toArray(arr);
            tBean = new TagBean(arr);
            tBean.setHasChanged(true);
            try {
                Util.saveSerializableData(mContext, tBean,
                        Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
            } catch (Exception e) {
                // TODO: handle exception
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove one tag of the application
     * 
     * @param tag
     *            tag to be removed
     * @return true if removing is successful, otherwise false
     */
    public boolean removeTag(String tag) {
        boolean success = false;
        if (tag != null && tag.length() != 0) {
            success = removeTags(new String[] { tag });
        }
        return success;
    }

    /**
     * Remove tags of the application
     * 
     * @param tags
     *            tags to be removed
     * @return true if removing is successful, otherwise false
     */
    public boolean removeTags(String[] tags) {
        if (tags == null || tags.length <= 0) {
            return false;
        }

        TagBean tBean = null;
        try {
            tBean = (TagBean) Util.getSerializableData(mContext,
                    Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
        } catch (Exception e) {
            tBean = null;
        }

        int curTagsNum = 0;
        Set<String> hSet = new HashSet<String>();
        if (tBean != null && tBean.getTags() != null) {
            curTagsNum = tBean.getTags().length;
            hSet.addAll(Arrays.asList(tBean.getTags()));
        } else {
            return false;
        }

        hSet.removeAll(Arrays.asList(tags));
        if (curTagsNum != hSet.size()) {
            try {
                String[] arr = new String[hSet.size()];
                hSet.toArray(arr);
                tBean = new TagBean(arr);
                tBean.setHasChanged(true);
                Util.saveSerializableData(mContext, tBean,
                        Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
                return true;
            } catch (Exception e) {
                // TODO: handle exception
                return false;
            }
        } else {
            return false;
        }
    }

    public String getTags() {
        TagBean tBean;
        try {
            tBean = (TagBean) Util.getSerializableData(mContext,
                    Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
        } catch (Exception e) {
            tBean = null;
        }

        if (tBean == null || tBean.getTags() == null || tBean.getTags().length == 0) {
            return "";
        }
        return tBean.toString();
    }

    /**
     * Check the tag has changed or not
     * 
     * @return true if the tag changed, otherwise false
     */
    public boolean hasTagsChanged() {
        TagBean tBean = null;
        try {
            tBean = (TagBean) Util.getSerializableData(mContext,
                    Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
        } catch (Exception e) {
            return false;
        }
        if (tBean == null) {
            return false;
        }
        return tBean.isHasChanged();
    }

    /**
     * Set the tags have uploaded
     */
    public void setTagsUploaded() {
        TagBean tBean = null;
        try {
            tBean = (TagBean) Util.getSerializableData(mContext,
                    Util.md5(mContext.getPackageName() + Constants.DROI_TAG_FILE_NAME));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (tBean == null) {
            return;
        } else if (!tBean.isHasChanged()) {
            return;
        } else {
            tBean.setHasChanged(false);
        }
    }

    /**
     * Setting push service available by user, the SDK would check this setting
     * if server setting is not exist or valid
     * 
     * @param isEnable
     *            whether the push service of this app is available
     * @return setting is successful or not
     */
    public boolean setPushEnabled(boolean isEnable) {
        ClientPushableBean bean = new ClientPushableBean(isEnable);
        try {
            Util.saveSerializableData(mContext, bean, Util.md5(mContext.getPackageName() + Constants.DROI_CLIENT_PH));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Check whether the push service is available for this application
     * 
     * @return true push service available, otherwise not available
     */
    public boolean getIsPushEnabled() {
        return isPushEnabled();
    }

    /**
     * Check the app can be pushed or not
     * 
     * @return true if app can be pushed, otherwise not
     */
    public boolean isPushEnabled() {
        ClientPushableBean bean = null;
        try {
            bean = (ClientPushableBean) Util.getSerializableData(mContext,
                    Util.md5(mContext.getPackageName() + Constants.DROI_CLIENT_PH));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (bean != null) {
            return bean.isPushable();
        }
        return true;
    }

    /**
     * Setting the heart beat interval for push service
     * 
     * @param second
     *            heart beat interval
     * @return setting result
     */
    private boolean saveServerHeart(int second) {
        if (second <= 0 || second >= Constants.MAX_HEARTBEAT_INTERVAL) {
            PushLog.e("Invalid heartbeat: " + second);
            return false;
        }

        try {
            Util.saveSerializableToSD(new HeartBeatBean(second), Util.md5(Constants.DROI_SERVER_HB));
        } catch (Exception e) {
            PushLog.e(e);
            return false;
        }
        return true;
    }

    /**
     * Check current device is recoverable or not
     * 
     * @return true we can push message to this device, otherwise not
     */
    public boolean isDeviceRecovered() {
        RecoveryTimeBean rBean;
        try {
            rBean = (RecoveryTimeBean) Util.getSerializableBySD(Util.md5(Constants.DROI_SERVER_RT));
        } catch (Exception e) {
            PushLog.e(e);
            rBean = null;
        }

        if (rBean != null) {
            return rBean.isRecovered();
        }
        return true;
    }

    /**
     * Check the app to the given package is recoverable or not
     * 
     * @param packageName
     *            the package name of the app
     * @return true if we can push message to this app, otherwise not
     */
    public boolean isAppRecovered(String packageName) {
        RecoveryTimeBean rBean;
        if (!Util.isStringValid(packageName)) {
            return false;
        }
        try {
            rBean = (RecoveryTimeBean) Util.getSerializableBySD(Util.md5(packageName + Constants.DROI_SERVER_RT));
        } catch (Exception e) {
            PushLog.e(e);
            rBean = null;
        }

        if (rBean != null) {
            return rBean.isRecovered();
        }
        return true;
    }

    /**
     * Save the silent time which is given by user for the app
     * 
     * @param startHour
     *            start hour
     * @param startMin
     *            start minute
     * @param endHour
     *            end hour
     * @param endMin
     *            end minute
     * @return whether the setting is successful
     */
    public boolean saveSilentTime(String packageName, boolean fromServer, int startHour, int startMin, int endHour,
            int endMin) {
        if (startHour < 0 || startMin < 0 || endHour < 0 || endMin < 0) {
            return false;
        }
        if (startHour == 0 && startMin == 0 && endHour == 0 && endMin == 0) {
            return false;
        }
        if (startHour >= 24 || startMin >= 60 || endHour >= 24 || endMin >= 60) {
            return false;
        }
        if (!Util.isStringValid(packageName)) {
            return false;
        }

        SlienceTimeBean sBean = new SlienceTimeBean(startHour, startMin, endHour, endMin);

        try {
            if (fromServer) {
                Util.saveSerializableData(mContext, sBean, Util.md5(packageName + Constants.DROI_SERVER_ST));
            } else {
                Util.saveSerializableData(mContext, sBean, Util.md5(packageName + Constants.DROI_CLIENT_ST));
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public int[] getAppSettingSilentTime(String packageName) {
        int[] silentTime = null;
        SlienceTimeBean sBean = null;

        try {
            sBean = (SlienceTimeBean) Util.getSerializableData(mContext, Util.md5(packageName + Constants.DROI_CLIENT_ST));
        } catch (Exception e) {
            PushLog.e("App setting silent time for " + packageName + " do not exist!");
        }

        if (sBean != null) {
            int time0 = sBean.getStartHour();
            int time1 = sBean.getStartMin();
            int time2 = sBean.getEndHour();
            int time3 = sBean.getEndMin();

            if (time0 < 0 || time0 > 23 || time2 < 0 || time2 > 23 || time1 < 0 || time1 > 59 || time3 < 0
                    || time3 > 59) {
                return silentTime;
            }

            silentTime = new int[4];
            silentTime[0] = time0;
            silentTime[1] = time1;
            silentTime[2] = time2;
            silentTime[3] = time3;
        }
        return silentTime;
    }

    public int[] getServerSettingSilentTime(String packageName) {
        int[] silentTime = null;
        SlienceTimeBean sBean = null;

        if (!Util.isStringValid(packageName)) {
            return silentTime;
        }

        try {
            sBean = (SlienceTimeBean) Util.getSerializableData(mContext, Util.md5(packageName + Constants.DROI_SERVER_ST));
        } catch (Exception e) {
            PushLog.e("Server setting silent time for " + packageName + " do not exist!");
        }

        if (sBean != null) {
            int time0 = sBean.getStartHour();
            int time1 = sBean.getStartMin();
            int time2 = sBean.getEndHour();
            int time3 = sBean.getEndMin();

            if (time0 < 0 || time0 > 23 || time2 < 0 || time2 > 23 || time1 < 0 || time1 > 59 || time3 < 0
                    || time3 > 59) {
                return silentTime;
            }

            silentTime = new int[4];
            silentTime[0] = time0;
            silentTime[1] = time1;
            silentTime[2] = time2;
            silentTime[3] = time3;
        }
        return silentTime;
    }

    /**
     * Get silent time for push service. This method get server setting silent
     * time first and use client setting data if previous one not exist
     * 
     * @return silent time slot which is a integer array, which saved start
     *         hour, start minute, end hour, end minute in order
     */
    public int[] getSilentTime(String packageName) {
        int[] silentTime = null;

        if (!Util.isStringValid(packageName)) {
            return silentTime;
        }

        silentTime = getServerSettingSilentTime(packageName);
        if (silentTime == null) {
            silentTime = getAppSettingSilentTime(packageName);
        }

        return silentTime;
    }

    /**
     * Check is in silent time or not
     * 
     * @return true if silent now, otherwise not
     */
    public boolean isSilentNow(String packageName) {
        if (!Util.isStringValid(packageName)) {
            return false;
        }

        int[] times = getSilentTime(packageName);
        if (times == null) {
            return false;
        } else {
            times[0] = (times[0] == 0 ? 24 : times[0]);
            times[2] = (times[2] == 0 ? 24 : times[2]);

            boolean afterOneDay = false;
            int smallHour = times[0];
            int smallMin = times[1];
            int bigHour = times[2];
            int bigMin = times[3];

            if (times[0] > times[2] || ((times[0] == times[2]) && (times[1] > times[3]))) {
                afterOneDay = true;
                smallHour = times[2];
                smallMin = times[3];
                bigHour = times[0];
                bigMin = times[1];
            }

            Calendar now = Calendar.getInstance();
            int curHour = now.get(Calendar.HOUR_OF_DAY);
            int curMinute = now.get(Calendar.MINUTE);
            boolean isInAscendingTimeSlot = true;
            if (curHour >= smallHour && curHour <= bigHour) {

                if (curHour == smallHour) {
                    isInAscendingTimeSlot = afterOneDay ? (curMinute > smallMin) : (curMinute >= smallMin);
                }
                if (curHour == bigHour && isInAscendingTimeSlot) {
                    isInAscendingTimeSlot = afterOneDay ? (curMinute < bigMin) : (curMinute <= bigMin);
                }

            } else {
                isInAscendingTimeSlot = false;
            }

            if (afterOneDay) {
                return !isInAscendingTimeSlot;
            } else {
                return isInAscendingTimeSlot;
            }
        }
    }

    /**
     * Get heart beat interval, the order is: server > user > default
     * 
     * @return current heart beat interval
     */
    public int getHeartBeat() {
        int heartBeat = 0;
        HeartBeatBean hBean;
        try {
            hBean = (HeartBeatBean) Util.getSerializableBySD(Util.md5(Constants.DROI_SERVER_HB));
        } catch (Exception e) {
            hBean = null;
        }
        if (hBean != null && hBean.getTime() != 0) {
            heartBeat = hBean.getTime();
            return heartBeat;
        } else {
            return Constants.DEFAULT_HEARTBEAT_INTERVAL;
        }
    }

    /**
     * @param packageName
     *            the package name message send to
     * @param appKey
     *            key of app, the appKey of this device is full zero
     * @param durationStr
     *            duration
     */
    void saveRecoveryTime(String packageName, String appKey, String durationStr) {
        long durationInMillisec;

        if (!Util.isStringValid(appKey) || !Util.isStringValid(durationStr) || appKey.length() != 32) {
            return;
        }

        if ("0".equals(durationStr)) {// offline forever
            durationInMillisec = Long.MAX_VALUE;
        } else if (KEY_SILENT_RECOVERY.equals(durationStr)) {
            durationInMillisec = EMPTY_RECOVERY_TIME;
        } else {
            durationInMillisec = getOfflineDuration(durationStr);
        }

        RecoveryTimeBean rBean = new RecoveryTimeBean(durationInMillisec);
        try {
            if (DEVICE_APP_KEY.equals(appKey)) {
                Util.saveSerializableToSD(rBean, Util.md5(Constants.DROI_SERVER_RT));
            } else {
                if (!Util.isStringValid(packageName)) {
                    return;
                }
                Util.saveSerializableData(mContext, rBean, Util.md5(packageName + Constants.DROI_SERVER_RT));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Transfer offline duration into million seconds
     * 
     * @param time
     * @return
     */
    public static long getOfflineDuration(String time) {
        long result = -1;
        try {
            result = Long.parseLong(time) * 60000L;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Save the silent time from server
     * 
     * @param sTimeStr
     * @return true parse and save successfully false parse error or save error
     */
    boolean processSlientTime(PushMsg msg) {
        if (msg == null) {
            return false;
        }

        String sTimeStr = msg.cmd_data;
        String packageName = msg.packageName;
        if (!Util.isStringValid(sTimeStr) || !Util.isStringValid(packageName)) {
            return false;
        }

        int startHour = -1;
        int startMin = -1;
        int endHour = -1;
        int endMin = -1;
        String[] times = sTimeStr.split(",");
        if (times.length != 4) {
            return false;
        }

        try {
            startHour = Integer.valueOf(times[0]);
            startMin = Integer.valueOf(times[1]);
            endHour = Integer.valueOf(times[2]);
            endMin = Integer.valueOf(times[3]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (startHour < 0 || startMin < 0 || endHour < 0 || endMin < 0) {
            return false;
        }
        if (startHour >= 24 || startMin >= 60 || endHour >= 24 || endMin >= 60) {
            return false;
        }
        return saveSilentTime(packageName, true, startHour, startMin, endHour, endMin);
    }

    /**
     * Save heart beat interval
     * 
     * @param hbStr
     *            heart beat time interval
     * @return
     */
    boolean processHeartBeat(String hbStr) {
        int heartBeat = 0;
        if (!Util.isStringValid(hbStr)) {
            return false;
        }
        try {
            heartBeat = Integer.valueOf(hbStr);
        } catch (Exception e) {
            heartBeat = -1;
        }

        if (heartBeat <= 0) {
            return false;
        }

        boolean result = true;
        int curHeartbeat = getHeartBeat();
        if (heartBeat != curHeartbeat) {
            result = saveServerHeart(heartBeat);

            Intent intent = new Intent();
            intent.setAction(Constants.INTENT_PUSHSERVICE);
            intent.putExtra(Constants.KEY_CMD, Constants.KEY_HEART_BEAT);
            PushUtil.startPushService(mContext, intent);
            PushLog.i("setServiceHB set successful...");
            return result;
        }

        return result;
    }

    /**
     * Save ip address passed from server
     * 
     * @param ips
     * @return
     */
    String[] udpAddress = null;

    public boolean saveAndResetIpAddress(List<String> udpAddressList, List<String> httpAddressList) {
        String[] httpAddress = null;
        if (udpAddressList != null) {
            udpAddress = new String[udpAddressList.size()];
            udpAddressList.toArray(udpAddress);
        }
        if (httpAddressList != null) {
            httpAddress = new String[httpAddressList.size()];
            httpAddressList.toArray(httpAddress);
        }

        ServiceIpsBean ipsBean = new ServiceIpsBean(udpAddress, httpAddress);

        String file_name = Constants.DROI_SERVER_IP;
        try {
            file_name = Util.md5(file_name);
        } catch (Exception e) {
            file_name = null;
        }

        if (ipsBean != null && file_name != null) {
            Util.saveSerializableToSD(ipsBean, file_name);

            String mode = Util.getTransferMode(mContext);
            Intent intent = new Intent();
            intent.setAction(Constants.INTENT_PUSHSERVICE);
            if (mode != null && mode.equals(Constants.TRANSFER_MODE_TCP)) {
                intent.putExtra(Constants.KEY_CMD, Constants.KEY_RESET_TCP);
            } else {
                intent.putExtra(Constants.KEY_CMD, Constants.KEY_RESET_UDP);
            }
            PushUtil.startPushService(mContext, intent);
            return true;
        }
        return false;
    }
}