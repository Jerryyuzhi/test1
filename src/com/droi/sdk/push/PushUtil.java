package com.droi.sdk.push;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.droi.sdk.push.DroiPushService.RunnableCallback;
import com.droi.sdk.push.utils.DeviceInfo;
import com.droi.sdk.push.utils.DroiPushBridge;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Pair;

class PushUtil {

    private static final String KEY_RESPONOSE_CODE = "rCode";

    static Runnable getRunnable(final Context context, final String urlString, final String appKey,
            final String sendData, final String desKey, final String deviceId, final RunnableCallback callback) {
        Runnable runnable = null;

        if (context == null) {
            PushLog.e("getServerIpRunnable: context is null");
            return runnable;
        }

        if (!Util.isStringValid(appKey)) {
            PushLog.e("getServerIpAddress: appKey invalid(" + appKey + ")");
            return runnable;
        }
        if (!Util.isStringValid(deviceId)) {
            PushLog.e("getServerIpAddress: deviceId invalid(" + deviceId + ")");
            return runnable;
        }
        if (!Util.isStringValid(desKey)) {
            PushLog.e("getServerIpAddress: desKey invalid(" + desKey + ")");
            return runnable;
        }

        runnable = new Runnable() {
            private RunnableCallback mCallback = callback;

            @Override
            public void run() {
                // TODO Auto-generated method stub
                String ret = null;
                try {
                    ret = Util.postData(context, urlString, sendData, desKey, appKey);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    PushLog.e(e);
                    return;
                }

                JSONObject retJson = null;
                if (ret != null) {
                    try {
                        retJson = new JSONObject(ret);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        PushLog.e(e);
                        return;
                    }
                } else {
                    return;
                }

                String result = null;
                JSONObject data = null;
                boolean success = false;
                if (retJson != null) {
                    try {
                        result = retJson.getString(Constants.KEY_RESULT);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        PushLog.e(e);
                    }

                    try {
                        data = retJson.getJSONObject(Constants.KEY_DATA);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        PushLog.e(e);
                    }
                }

                success = Constants.KEY_RESPONSE_SUCCESS.equals(result) ? true : false;
                mCallback.onRunnableResult(success, data);
            }
        };
        return runnable;
    }

    static long[] getNextSilentTime(int startHour, int startMinute, int endHour, int endMinute) {
        long[] nextSilentTime = new long[] { -1, -1 };
        Date now = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.set(Calendar.HOUR_OF_DAY, startHour);
        calendar.set(Calendar.MINUTE, startMinute);
        nextSilentTime[0] = calendar.getTimeInMillis();
        Date start = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, endHour);
        calendar.set(Calendar.MINUTE, endMinute);
        Date end = calendar.getTime();

        if (start.after(end)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        nextSilentTime[1] = calendar.getTimeInMillis();

        return nextSilentTime;
    }

    static void savePushMessageIfNecessary(Context context, PushMsg pmsg) {
        if (pmsg == null || !pmsg.valid) {
            return;
        }

        boolean isSilentMode = PushSetting.getInstance(context).isSilentNow(pmsg.packageName);
        if (isSilentMode) {
            if (pmsg.isDisposableMessage()) {
                int[] silentTime = PushSetting.getInstance(context).getSilentTime(pmsg.packageName);
                if ((silentTime.length == 4) && (silentTime[0] > 0) && (silentTime[1] > 0) && (silentTime[2] > 0)
                        && (silentTime[2] > 0)) {
                    long[] silentDuration = getNextSilentTime(silentTime[0], silentTime[1], silentTime[2],
                            silentTime[3]);
                    pmsg.time_1 = silentDuration[1];
                    pmsg.time_2 = pmsg.time_1 + Constants.DEF_TIME_PERIOD;
                }
            } else if (pmsg.isTimingMessage()) {
                int[] silentTime = PushSetting.getInstance(context).getSilentTime(pmsg.packageName);
                if ((silentTime.length == 4) && (silentTime[0] > 0) && (silentTime[1] > 0) && (silentTime[2] > 0)
                        && (silentTime[2] > 0)) {
                    long[] silentDuration = getNextSilentTime(silentTime[0], silentTime[1], silentTime[2],
                            silentTime[3]);
                    if (pmsg.time_2 <= silentDuration[1]) {
                        return;
                    } else if (pmsg.time_1 < silentDuration[1]) {
                        pmsg.time_2 = silentDuration[1] + pmsg.time_2 - pmsg.time_1;
                        pmsg.time_1 = silentDuration[1];
                    }
                }
            }
            DroiPush.mManager.savePushMsg(pmsg);
        } else if (pmsg.isTimingMessage() || pmsg.isCyclicMessage()) {
            DroiPush.mManager.savePushMsg(pmsg);
        }
    }

    static void saveDistributedIpAddress(Context context, JSONObject data) {
        JSONArray ipList = null;
        try {
            ipList = data.getJSONArray(Constants.KEY_ADDRESS_LIST);
            if (ipList != null) {
                ArrayList<String> httpAddress = new ArrayList<String>();
                ArrayList<String> udpAddress = new ArrayList<String>();
                for (int i = 0; i < ipList.length(); i++) {
                    JSONObject object = (JSONObject) ipList.get(i);
                    String type = object.getString(Constants.KEY_MODULE_ID);
                    String host = object.getString(Constants.KEY_HOST);
                    String port = object.getString(Constants.KEY_PORT);

                    if (!Util.isStringValid(host) || !Util.isStringValid(port)) {
                        continue;
                    }
                    if (type.equals(Constants.MODULE_HTTP)) {// http
                                                          // address
                        httpAddress.add(host + ":" + port);
                    } else if (type.equals(Constants.MODULE_UDP)) {// udp
                                                                // address
                        udpAddress.add(host + ":" + port);
                    }
                }
                PushSetting.getInstance(context).saveAndResetIpAddress(udpAddress, httpAddress);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
            return;
        }
    }

    static void uploadAppInfo(final Context ctx) {
        final String appKey = Util.getAppKey(ctx);
        final String secret = Util.getSecret(ctx);
        final PushSetting setter = PushSetting.getInstance(ctx);
        final String tags = setter.getTags();
        if (appKey != null) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    String ret;
                    try {
                        String sendData = Util.getAppInfo(ctx, tags);
                        PushLog.v("SendAppInfo: " + sendData);
                        ret = Util.postData(ctx, Constants.APP_INFO_UPLOAD_SERVER, sendData, secret, appKey);

                        PushLog.v("AppInfo:" + ret);
                        JSONObject jsonobj = null;
                        jsonobj = new JSONObject(ret);
                        int rcode = jsonobj.getInt(KEY_RESPONOSE_CODE);
                        if (rcode == 200) {
                            SharedPreferences account = ctx.getSharedPreferences(Constants.DEFAULT_PRE_NAME,
                                    Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = account.edit();
                            editor.putBoolean(Constants.REG_APPINFO_STATUS, true);
                            editor.commit();
                            setter.setTagsUploaded();
                            PushLog.v("Upload AppInfo Success");
                        }
                    } catch (IOException e) {
                        PushLog.e(e);
                    } catch (JSONException e) {
                        PushLog.e(e);
                    }
                }
            }).start();
        }
        return;
    }

    static void uploadDeviceInfo(final Context context) {
        final String info = DeviceInfo.get(context);
        final String appKey = Util.getAppKey(context);
        final String secret = Util.getSecret(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                String ret;
                try {
                    PushLog.v(info);
                    ret = Util.postData(context, Constants.DEV_INFO_UPLOAD_SERVER, info, secret, appKey);
                    PushLog.v("DevInfo:" + ret);
                    JSONObject jsonobj = null;
                    jsonobj = new JSONObject(ret);
                    int rcode = jsonobj.getInt(KEY_RESPONOSE_CODE);
                    if (rcode == 200) {
                        SharedPreferences account = context.getSharedPreferences(Constants.DEFAULT_PRE_NAME,
                                Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = account.edit();
                        editor.putBoolean(Constants.REG_DEVINFO_STATUS, true);
                        editor.commit();
                        PushLog.v("Upload DevInfo Success");
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    PushLog.e(e);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    PushLog.e(e);
                }
            }
        }).start();
    }

    static void startPushService(Context context, Intent intent) {
        Pair<String, String> runningService = Util.getRunningPushService(context);
        if (runningService != null) {
            String curPackageName = context.getApplicationContext().getPackageName();
            // running service is not the current, clear and stop current
            // service now
            if (!curPackageName.equals(runningService.first)) {
                // clear and stop current service
                DroiPush.clearPushEnvironment();
                Intent stopSrv = new Intent();
                stopSrv.setAction(Constants.INTENT_PUSHSERVICE);
                stopSrv.setPackage(context.getPackageName());
                context.stopService(stopSrv);

                // passed intent to running service
                Intent startSrv = new Intent(intent);
                ComponentName cn = new ComponentName(runningService.first, runningService.second);
                startSrv.setComponent(cn);
                context.startService(startSrv);
            } else {// current service is running
                    // current service is the latest?
                Pair<String, String> latestService = Util.getLatestServiceInfo(context);
                if (latestService != null && !latestService.first.equals(runningService.first)) {
                    // clear and stop current service
                    DroiPush.clearPushEnvironment();
                    Intent stopSrv = new Intent();
                    stopSrv.setAction(Constants.INTENT_PUSHSERVICE);
                    stopSrv.setPackage(context.getPackageName());
                    context.stopService(stopSrv);

                    // set up the latest
                    if (intent != null) {
                        Intent startSrv = new Intent(intent);
                        ComponentName cn = new ComponentName(latestService.first, latestService.second);
                        startSrv.setComponent(cn);
                        context.startService(startSrv);
                    }
                } else {// current service is the latest service
                    if (intent != null) {
                        Intent startSrv = new Intent(intent);
                        startSrv.setAction(Constants.INTENT_PUSHSERVICE);
                        startSrv.setPackage(context.getPackageName());
                        context.startService(startSrv);// pass intent
                    }
                }
            }
        } else {// no service is running now, set up the latest service
            if (intent != null) {
                Pair<String, String> latestService = Util.getLatestServiceInfo(context);
                Intent startSrv = new Intent(intent);
                ComponentName cn = new ComponentName(latestService.first, latestService.second);
                startSrv.setComponent(cn);
                context.startService(startSrv);
            }
        }
    }

    static String decodeMessage(byte[] data, String secret) {
        byte[] decryptedData = DroiPushBridge.decryptData(secret, data);
        if (decryptedData == null) {
            return null;
        }
        // notice for padding
        int i;
        for (i = decryptedData.length - 1; i >= 0; i--) {
            if (decryptedData[i] != 0) {
                break;
            }
        }

        byte[] decryptedDataNoZero = new byte[i + 1];
        System.arraycopy(decryptedData, 0, decryptedDataNoZero, 0, i + 1);

        String decoded = null;
        try {
            decoded = new String(decryptedDataNoZero, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }

        return decoded;
    }
}