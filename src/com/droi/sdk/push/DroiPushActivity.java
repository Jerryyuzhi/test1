package com.droi.sdk.push;

import com.droi.sdk.DroiException;
import com.droi.sdk.push.download.DownloadTaskManager;
import com.droi.sdk.push.utils.AppDetailView;
import com.droi.sdk.push.utils.CPResourceUtil;
import com.droi.sdk.push.utils.DroiPushBridge;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import com.droi.sdk.utility.Utility;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

public class DroiPushActivity extends Activity {

    static final String KEY_MESSAGE_ID = "msg_id";
    static final String KEY_REQUEST_ID = "request_id";
    static final String KEY_IMAGE_URL = "image_url";
    static final String KEY_PACKAGE_NAME = "pkg_name";
    static final String KEY_DOWNLOAD_URL = "download_url";
    private final String KEY_CANCEL_DRAWABLE_ID = "dp_close_dialog_btn";

    private AppDetailView mAppView;
    private ImageView mCancelBtn;
    private long mMessageId = -1;

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mMessageId = intent.getLongExtra(KEY_MESSAGE_ID, -1);
        final int requestId = intent.getIntExtra(KEY_REQUEST_ID, -1);
        String imagePath = intent.getStringExtra(KEY_IMAGE_URL);
        final String pkgName = intent.getStringExtra(KEY_PACKAGE_NAME);
        final String downloadUrl = intent.getStringExtra(KEY_DOWNLOAD_URL);

        if (!Util.isStringValid(pkgName) || !Util.isStringValid(downloadUrl)) {
            PushLog.e("Package name or download url is valid!");
            finish();
            return;
        }

        if (Util.isStringValid(imagePath)) {
            boolean isCached = false;
            try {
                isCached = Utility.isBitmapCached(imagePath);
            } catch (DroiException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                isCached = false;
            }

            if (isCached) {
                Bitmap bitmap = null;
                try {
                    bitmap = Utility.getBitmap(imagePath, 0, 0);
                } catch (DroiException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    bitmap = null;
                }

                if (bitmap == null) {
                    DownloadTaskManager.getInstance(DroiPushActivity.this).startDownload(mMessageId, requestId, downloadUrl,
                            pkgName);
                    finish();
                    return;
                }

                FrameLayout container = new FrameLayout(getApplicationContext());

                mAppView = new AppDetailView(this, bitmap);
                mAppView.setDownloadListener(new DownloadListener() {
                    @Override
                    public void onDownloadStart() {
                        // TODO Auto-generated method stub
                        //send analytics info
                        if (mMessageId > 0) {
                            //send analytics info
                            String deviceId = DroiPushBridge.generateUUID(DroiPushActivity.this);
                            String header = Util.buildAnalyticsHeader();
                            String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, mMessageId, Constants.ANALYTICS_CLICK_DOWNLOAD_BTN, deviceId);
                            Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
                        }

                        DownloadTaskManager.getInstance(DroiPushActivity.this).startDownload(mMessageId, requestId, downloadUrl,
                                pkgName);
                        finish();
                    }

                });

                LayoutParams lp1 = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lp1.gravity = Gravity.CENTER;
                mAppView.setLayoutParams(lp1);
                container.addView(mAppView);

                mCancelBtn = new ImageView(this);
                int delResId = CPResourceUtil.getDrawableId(this, KEY_CANCEL_DRAWABLE_ID);
                if (delResId != 0) {
                    mCancelBtn.setImageResource(delResId);
                } else {
                    PushLog.e("Can not find the resource of cancel button!");
                }

                LayoutParams lp2 = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lp2.gravity = Gravity.RIGHT | Gravity.TOP;
                lp2.setMargins(0, 10, 10, 0);
                mCancelBtn.setLayoutParams(lp2);
                container.addView(mCancelBtn);
                mCancelBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (mMessageId > 0) {
                            //send analytics info
                            String deviceId = DroiPushBridge.generateUUID(DroiPushActivity.this);
                            String header = Util.buildAnalyticsHeader();
                            String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, mMessageId, Constants.ANALYTICS_CLICK_CANCEL_BTN, deviceId);
                            Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
                        }
                        finish();
                    }
                });

                if (mMessageId > 0) {
                    String deviceId = DroiPushBridge.generateUUID(DroiPushActivity.this);
                    String header = Util.buildAnalyticsHeader();
                    String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, mMessageId, Constants.ANALYTICS_SHOW_APP_DETAIL, deviceId);
                    Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
                }

                setContentView(container);
            } else {
                PushLog.e("Bitmap is not cached!");
                DownloadTaskManager.getInstance(DroiPushActivity.this).startDownload(mMessageId, requestId, downloadUrl, pkgName);
                finish();
                return;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && isOutOfBounds(this, event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    private boolean isOutOfBounds(Activity context, MotionEvent event) {
        final int x = (int) event.getX();
        final int y = (int) event.getY();
        final int slop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
        final View decorView = context.getWindow().getDecorView();
        return (x < -slop) || (y < -slop) || (x > (decorView.getWidth() + slop))
                || (y > (decorView.getHeight() + slop));
    }

    public interface DownloadListener {
        public void onDownloadStart();
    }
}
