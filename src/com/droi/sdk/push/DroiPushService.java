package com.droi.sdk.push;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Pair;
import com.droi.sdk.push.PushMsg.ImageLoadListener;
import com.droi.sdk.push.core.Message;
import com.droi.sdk.push.core.UDPClientBase;
import com.droi.sdk.push.core.WapClientBase;
import com.droi.sdk.push.data.DatabaseManager;
import com.droi.sdk.push.utils.DroiPushBridge;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;

public class DroiPushService extends Service {
    private static final int MSG_RECEIVE_MESSAGE = 1;
    private static final int MSG_RECEIVE_TICK = 2;
    public static final String KEY_PUSH_MESSAGE_BYTE = "push_message_byte";
    public static final String KEY_PUSH_MESSAGE_STRING = "push_message_string";
    static final String KEY_MESSAGE = "msg";
    static final String KEY_MESSAGE_TAG = "tag";
    static final String KEY_APP_KEY = "app_key";
    static final String KEY_SECRET = "secret";
    static final String KEY_PACKAGE = "package";
    private static final String KEY_MSG_ID = "msg_id";

    private Context mContext;
    protected PendingIntent tickPendIntent;
    private WakeLock wakeLock;
    private WorkHandler mWorkHandler;
    private PushMessageFilter mFilter;
    private Map<String, PushMsg> mLoadingPushMsgs = new HashMap<String, PushMsg>();
    private HashMap<String, Pair<String, String>> mAppInfo;

    private boolean isOnCreate = false;

    private void initService(Context context) {
        if (DroiPush.mManager == null) {
            DroiPush.mManager = DatabaseManager.getInstance(context);
        }

        if (DroiPush.mLooper == null) {
            HandlerThread thread = new HandlerThread("DroiPush");
            thread.start();
            DroiPush.mLooper = thread.getLooper();
        }
        mWorkHandler = new WorkHandler(DroiPush.mLooper);

        if (mFilter == null) {
            mFilter = new PushMessageFilter();
        }

        if (DroiPush.mPool == null) {
            DroiPush.mPool = Executors.newFixedThreadPool(5);
        }

        mAppInfo = DroiPush.mManager.getAppInfo();
    }

    @Override
    public void onCreate() {
        mContext = getApplicationContext();
        isOnCreate = true;
    }

    @Override
    public int onStartCommand(Intent param, int flags, int startId) {
        PushLog.i("PushService onStartCommand: " + getPackageName());

        Pair<String, String> latest = Util.getLatestServiceInfo(mContext);
        if (latest != null) {
            String pName = latest.first;
            String curPname = getPackageName();
            if (!curPname.equals(pName)) {
                PushUtil.startPushService(mContext, param);
                return START_NOT_STICKY;
            }
        }

        if (isOnCreate) {
            initService(mContext);
            setTickAlarm();
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "OnlineService");
            // Reset Client when Service start at first time.
            // This function call is required, as the service will be restarted
            // after it killed
            // by OS. And there is no "CMD" key when restart.
            resetClient(false);

            isOnCreate = false;
        }

        if (param == null) {
            return START_STICKY;
        }

        String cmd = param.getStringExtra(Constants.KEY_CMD);
        if (Util.isStringValid(cmd)) {
            if (Constants.KEY_NETWORK_CHANGE.equals(cmd)) {
                resetClient(true);
            } else if (Constants.KEY_TICK.equals(cmd)) {
                if (wakeLock != null && wakeLock.isHeld() == false) {
                    wakeLock.acquire();
                }
                if (mWorkHandler != null) {
                    android.os.Message msg = mWorkHandler.obtainMessage(MSG_RECEIVE_TICK);
                    mWorkHandler.sendMessage(msg);
                }
            } else if (Constants.KEY_HEART_BEAT.equals(cmd)) {
                if (DroiPush.myUdpClient != null) {
                    DroiPush.myUdpClient.setHeartbeatInterval(PushSetting.getInstance(mContext).getHeartBeat());
                }
            } else if (Constants.KEY_RESET_UDP.equals(cmd) || Constants.KEY_RESET_TCP.equals(cmd)) {
                // use udp mode all the time
                if (wakeLock != null && wakeLock.isHeld() == false) {
                    wakeLock.acquire();
                }
                resetClient(true);
                onPushInit(param);
            }
        }

        return START_STICKY;
    }

    protected void setTickAlarm() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, TickAlarmReceiver.class);
        int requestCode = 0;
        tickPendIntent = PendingIntent.getBroadcast(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long triggerAtTime = System.currentTimeMillis();
        int interval = 300 * 1000;
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtTime, interval, tickPendIntent);
    }

    protected void cancelTickAlarm() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmMgr.cancel(tickPendIntent);
    }

    private void onPushInit(Intent param) {
        String appkey = param.getStringExtra(KEY_APP_KEY);
        String secret = param.getStringExtra(KEY_SECRET);
        String packageName = param.getStringExtra(KEY_PACKAGE);
        if (Util.isStringValid(appkey) && Util.isStringValid(secret) && Util.isStringValid(packageName)) {
            addAppInfo(appkey, secret, packageName);
            if (DroiPush.mManager != null) {
                DroiPush.mManager.insertAppInfo(appkey, secret, packageName);
            }
        }
    }

    protected void resetClient(boolean resetByApp) {
        if (Util.isWapLink(mContext)) {
            if (DroiPush.myUdpClient != null) {
                DroiPush.myUdpClient.stop();
                DroiPush.myUdpClient = null;
            }
            if (DroiPush.myWapClient != null) {
                if (resetByApp) {
                    return;
                }
                DroiPush.myWapClient.stop();
            }
            String deviceId = DroiPushBridge.generateUUID(mContext);
            if (deviceId != null) {
                DroiPush.myWapClient = createWAPClient(mContext, deviceId);
                DroiPush.myWapClient.start();
            }
        } else {
            if (DroiPush.myWapClient != null) {
                DroiPush.myWapClient.stop();
                DroiPush.myWapClient = null;
            }
            String uuid = DroiPushBridge.generateUUID(mContext);
            byte[] bb = Util.string2bytes(uuid);
            if (DroiPush.myUdpClient != null) {
                // The socket connection will only reset by network change
                // or service restart
                // Every app start will cause the service receive
                // "RESET_UDP" cmd, but don't
                // actually need to reset the socket connection.
                if (resetByApp) {
                    return;
                }
                try {
                    DroiPush.myUdpClient.stop();
                } catch (Exception e) {
                }
            }

            PushLog.v("<=== UDP Client reset ===>");
            try {
                DroiPush.myUdpClient = createUDPClient(bb, 1);
                DroiPush.myUdpClient.setHeartbeatInterval(PushSetting.getInstance(mContext).getHeartBeat());
                DroiPush.myUdpClient.start();
            } catch (Exception e) {
            }
        }
    }

    protected void tryReleaseWakeLock() {
        if (wakeLock != null && wakeLock.isHeld() == true) {
            wakeLock.release();
        }
    }

    private void sendPushDataBroadCast(String appkey, PushMsg msg) {
        if (!Util.isStringValid(appkey) || !Util.isStringValid(msg.mMsgJson)) {
            return;
        }

        Intent intent = new Intent(Constants.INTENT_PUSHDATA);
        intent.putExtra(KEY_MESSAGE, msg.mMsgJson);
        intent.putExtra(KEY_APP_KEY, appkey);
        intent.putExtra(KEY_MESSAGE_TAG, msg.getTagIfCyclicMessage());

        if (Build.VERSION.SDK_INT >= 12) {
            intent.addFlags(32);
        }
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        clearEnvironment(true);
    }

    private void cancelNotifyRunning() {
        NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    class WorkHandler extends Handler {
        public WorkHandler(Looper loop) {
            super(loop);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            // TODO Auto-generated method stub
            switch (msg.what) {
            case MSG_RECEIVE_MESSAGE:
                dealwithReceivedMessage(msg.getData());
                break;

            case MSG_RECEIVE_TICK:
                resetClient(true);
                removeMessages(MSG_RECEIVE_TICK);
                boolean pushEnabled = PushSetting.getInstance(mContext).isDeviceRecovered();
                if (pushEnabled) {
                    showLocalStoredMessageIfNecessary();
                }
                // delete overdue push record
                mFilter.clearOverduePushRecord();
                break;
            default:
                break;
            }
        }
    }

    private void getShortLinkMessage(Context context, final String appKey, final String deviceId, final long msgId,
            final String url, final String desKey) {

        if (msgId < 0) {
            PushLog.e("getShortLinkMessage: msgId invalid(" + msgId + ")");
            return;
        }
        if (url == null || url.length() == 0) {
            PushLog.e("getShortLinkMessage: url invalid(" + url + ")");
            return;
        }

        String content = null;
        JSONObject obj = new JSONObject();
        try {
            obj.put(Constants.KEY_OS_TYPE, Constants.OSTYPE);
            obj.put(Constants.KEY_TIME_STAMP, System.currentTimeMillis() + "");
            obj.put(Constants.KEY_DEVICE_ID, deviceId);
            obj.put(Constants.KEY_SDK_VERSION, Constants.SDK_VERSION);
            obj.put(Constants.KEY_MSG_ID, msgId + "");
            content = obj.toString();
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return;
        }

        Runnable runnable = PushUtil.getRunnable(context, url, appKey, content, desKey, deviceId,
                new RunnableCallback() {

                    @Override
                    public void onRunnableResult(boolean isSuccess, JSONObject data) {
                        // TODO Auto-generated method stub
                        if (isSuccess) {
                            PushLog.i("Get short link message result: success!");
                            String msgInfo = null;
                            try {
                                msgInfo = data.getString(Constants.KEY_MESSAGE_INFO);
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                PushLog.e(e);
                                return;
                            }

                            if (msgInfo != null) {
                                android.os.Message msg = mWorkHandler.obtainMessage(MSG_RECEIVE_MESSAGE);
                                Bundle b = new Bundle();
                                b.putString(KEY_APP_KEY, appKey);
                                b.putString(KEY_PUSH_MESSAGE_STRING, msgInfo);
                                msg.setData(b);
                                mWorkHandler.sendMessage(msg);
                            }
                        } else {
                            PushLog.i("Get short link message result: failed!");
                            String errCode = null;
                            try {
                                errCode = data.getString(Constants.KEY_ERROR_CODE);
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                PushLog.e(e);
                            }

                            if (errCode != null) {
                                PushLog.e("Receive short link msg error, code = " + errCode);
                            }
                        }
                    }
                });

        if (runnable != null) {
            DroiPush.mPool.execute(runnable);
        }
    }

    private void getServerIpAddress(Context context) {
        String appKey = Util.getAppKey(context);
        String desKey = Util.getSecret(context);
        String deviceId = DroiPushBridge.generateUUID(mContext);
        String content = null;

        JSONObject obj = new JSONObject();
        try {
            obj.put(Constants.KEY_OS_TYPE, Constants.OSTYPE);
            obj.put(Constants.KEY_TIME_STAMP, System.currentTimeMillis() + "");
            obj.put(Constants.KEY_SDK_VERSION, Constants.SDK_VERSION);
            obj.put(Constants.KEY_DEVICE_ID, deviceId);
            content = obj.toString();
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return;
        }

        Runnable runnable = PushUtil.getRunnable(context, Constants.PUSH_RANDOM_IP_SERVER_SUFFIX, appKey, content, desKey,
                deviceId, new RunnableCallback() {

                    @Override
                    public void onRunnableResult(boolean isSuccess, JSONObject data) {
                        // TODO Auto-generated method stub
                        if (isSuccess) {
                            PushLog.i("getServerIpAddress: success!");
                            PushUtil.saveDistributedIpAddress(mContext, data);
                        } else {
                            PushLog.i("getServerIpAddress: failed!");
                            String errCode = null;
                            try {
                                errCode = data.getString(Constants.KEY_ERROR_CODE);
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                PushLog.e(e);
                            }
                            if (errCode != null) {
                                PushLog.e("Get server ip address error, code = " + errCode);
                            }
                        }
                    }
                });

        if (runnable != null) {
            DroiPush.mPool.execute(runnable);
        }
    }

    private void loadLoopMessageIfNecessary(final PushMsg msg) {
        if (msg == null) {
            return;
        }
        // delete message if app info is not exist
        String secret = getAppSecret(msg.app_key);
        String packageName = getAppPackage(msg.app_key);
        if (!Util.isStringValid(secret) || !Util.isStringValid(packageName)) {
            DroiPush.mManager.deleteMessageByTag(msg.tag);
            return;
        }

        msg.setPackageName(packageName);

        final String appKey = msg.app_key;
        final long msgId = msg.msg_id;
        final String deviceId = DroiPushBridge.generateUUID(mContext);

        String content = null;
        JSONObject obj = new JSONObject();
        try {
            obj.put(Constants.KEY_OS_TYPE, Constants.OSTYPE);
            obj.put(Constants.KEY_TIME_STAMP, System.currentTimeMillis() + "");
            obj.put(Constants.KEY_DEVICE_ID, deviceId);
            obj.put(Constants.KEY_SDK_VERSION, Constants.SDK_VERSION);
            obj.put(Constants.KEY_MESSAGE_ID, msgId + "");
            content = obj.toString();
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return;
        }

        Runnable runnable = PushUtil.getRunnable(mContext, Constants.MSG_VALIDITY_VERIFY_SERVER_SUFFIX, appKey, content,
                secret, deviceId, new RunnableCallback() {

                    @Override
                    public void onRunnableResult(boolean isSuccess, JSONObject data) {
                        // TODO Auto-generated method stub
                        if (isSuccess) {
                            PushLog.i("repeat message is valid: " + msg.msg_id);
                            loadPushMessage(msg);
                        } else {
                            PushLog.i("repeat message is invalid: " + msg.msg_id);
                            DroiPush.mManager.deleteMessageByTag(msg.tag);
                        }
                    }
                });

        if (runnable != null) {
            DroiPush.mPool.execute(runnable);
        }
    }

    private void showLocalStoredMessageIfNecessary() {
        List<PushMsg> list = DroiPush.mManager.getLocalStoredMessage();
        if (list == null) {
            return;
        }

        for (PushMsg s : list) {
            if (s.isValid()) {
                s.setPackageName(getAppPackage(s.app_key));
                if (s.showCyclicMessageNow()) {
                    loadLoopMessageIfNecessary(s);
                    continue;
                }
                if (s.showTimingMessageNow()) {
                    loadPushMessage(s);
                    continue;
                }
            }
            if (!s.isValid()) {
                DroiPush.mManager.deleteMessageByTag(s.tag);
            }
        }
    }

    private void loadPushMessage(PushMsg msg) {
        if (msg == null || !msg.isValid()) {
            return;
        }

        boolean isAppRecovered = PushSetting.getInstance(mContext).isAppRecovered(msg.packageName);
        boolean isAppSilent = PushSetting.getInstance(mContext).isSilentNow(msg.packageName);
        boolean isDeviceRecovered = PushSetting.getInstance(mContext).isDeviceRecovered();
        if (!isAppRecovered || isAppSilent || !isDeviceRecovered) {
            return;
        }

        if (msg.needDownloadImage()) {
            mLoadingPushMsgs.put(msg.tag, msg);
            msg.setImageLoadListener(new ImageLoadListener() {
                @Override
                public void onImageLoadResult(PushMsg msg, boolean result) {
                    // TODO Auto-generated method stub
                    if (result && msg.shouldShowMessageNow()) {
                        mLoadingPushMsgs.remove(msg.tag);
                        sendPushDataBroadCast(msg.app_key, msg);
                        if (msg.isTimingMessage()) {
                            DroiPush.mManager.deleteMessageByTag(msg.tag);
                        } else if (msg.isCyclicMessage()) {
                            DroiPush.mManager.updateLastShowTime(msg.tag, System.currentTimeMillis());
                        }
                    }
                }
            });
            msg.startDownloadResource();
        } else {
            if (msg.shouldShowMessageNow()) {
                sendPushDataBroadCast(msg.app_key, msg);
                if (msg.isTimingMessage()) {
                    DroiPush.mManager.deleteMessageByTag(msg.tag);
                } else if (msg.isCyclicMessage()) {
                    DroiPush.mManager.updateLastShowTime(msg.tag, System.currentTimeMillis());
                }
            }
        }
    }

    private void clearEnvironment(boolean cancelAlarm) {
        tryReleaseWakeLock();
        cancelNotifyRunning();
        this.tryReleaseWakeLock();

        if (cancelAlarm) {
            cancelTickAlarm();
        }

        if (DroiPush.myWapClient != null) {
            DroiPush.myWapClient.stop();
            DroiPush.myWapClient = null;
        }

        if (DroiPush.myUdpClient != null) {
            try {
                DroiPush.myUdpClient.stop();
                DroiPush.myUdpClient = null;
            } catch (Exception e) {
                PushLog.e(e);
            }
        }

        if (DroiPush.mManager != null) {
            DroiPush.mManager.onDestroy();
            DroiPush.mManager = null;
        }

        if (DroiPush.mLooper != null) {
            DroiPush.mLooper.quit();
            DroiPush.mLooper = null;
        }

        if (DroiPush.mPool != null) {
            DroiPush.mPool.shutdownNow();
            DroiPush.mPool = null;
        }

        if (mAppInfo != null) {
            mAppInfo.clear();
            mAppInfo = null;
        }
    }

    private WapClientBase createWAPClient(final Context context, final String deviceId) {
        WapClientBase instance = new WapClientBase(context, deviceId) {

            @Override
            public void trySystemSleep() {
                // TODO Auto-generated method stub
                tryReleaseWakeLock();
            }

            @Override
            public void onPushMessage(PushMsg msg) {
                // TODO Auto-generated method stub
                dispatchWAPMessage(msg);
            }

            @Override
            public boolean isNetworkAvailable() {
                // TODO Auto-generated method stub
                return Util.hasNetwork(DroiPushService.this);
            }

            @Override
            public boolean isMessageExist(PushMsg msg) {
                // TODO Auto-generated method stub
                return isMessageExist(msg);
            }
        };

        return instance;
    }

    private UDPClientBase createUDPClient(byte[] uuid, int appid) throws Exception {
        UDPClientBase instance = new UDPClientBase(uuid, appid) {

            @Override
            public void trySystemSleep() {
                // TODO Auto-generated method stub
                tryReleaseWakeLock();
            }

            @Override
            public void onPushMessage(Message message) {
                // TODO Auto-generated method stub
                dispatchUDPMessage(message);
            }

            @Override
            public boolean isNetworkAvaliable() {
                // TODO Auto-generated method stub
                return Util.hasNetwork(DroiPushService.this);
            }

            @Override
            public boolean isMessageExist(Message msg) {
                // TODO Auto-generated method stub
                return mFilter.isPushMessageExist(msg.getAppKey(), msg.getMsgId());
            }
        };

        return instance;
    }

    private void dispatchUDPMessage(Message message) {
        if (message == null || message.getData() == null || message.getData().length == 0) {
            return;
        }

        PushLog.i("onPushMessage: " + message.getMsgId());

        switch (message.getCmd()) {
        case Message.CMD_0x20:
            android.os.Message msg = mWorkHandler.obtainMessage(MSG_RECEIVE_MESSAGE);
            Bundle b = new Bundle();
            b.putLong(KEY_MSG_ID, message.getMsgId());
            b.putString(KEY_APP_KEY, message.getAppKey());
            b.putByteArray(KEY_PUSH_MESSAGE_BYTE, message.getContent());
            msg.setData(b);
            mWorkHandler.sendMessage(msg);
            break;

        case Message.CMD_0x10:
            break;

        case Message.CMD_0x11:
            break;

        case Message.CMD_0x40:
            break;

        default:
            break;
        }
    }

    private void dispatchWAPMessage(PushMsg pmsg) {
        if (pmsg == null) {
            return;
        }
        if (PushSetting.DEVICE_APP_KEY.equals(pmsg.app_key)) {
            parseContorlMsg(pmsg);
        } else {
            if (!mFilter.isPushMessageExist(pmsg.app_key, pmsg.msg_id)) {
                if (pmsg.valid) {
                    String packageName = getAppPackage(pmsg.app_key);
                    pmsg.setPackageName(packageName);
                    PushUtil.savePushMessageIfNecessary(mContext, pmsg);
                    loadPushMessage(pmsg);
                }
            }
        }
    }

    private void dealwithReceivedMessage(Bundle bundle) {
        byte[] data = null;
        String jsonStr = null;
        String secret = null;
        String packageName = null;
        String appKey;
        long msgId = -1;

        if (bundle == null) {
            return;
        }

        appKey = bundle.getString(KEY_APP_KEY);
        if (!Util.isStringValid(appKey)) {
            return;
        }

        // Control message to the whole device
        if (PushSetting.DEVICE_APP_KEY.equals(appKey)) {
            secret = PushSetting.PUBLIC_KEY;
            data = bundle.getByteArray(KEY_PUSH_MESSAGE_BYTE);
            if (data != null) {
                jsonStr = PushUtil.decodeMessage(data, secret);
                if (Util.isStringValid(jsonStr)) {
                    PushMsg msg = new PushMsg(jsonStr);
                    msg.app_key = PushSetting.DEVICE_APP_KEY;
                    parseContorlMsg(msg);
                }
            }
        } else {
            secret = getAppSecret(appKey);
            packageName = getAppPackage(appKey);
            if (!Util.isStringValid(secret) || !Util.isStringValid(packageName)) {
                return;
            }

            msgId = bundle.getLong(KEY_MSG_ID, -1);
            data = bundle.getByteArray(KEY_PUSH_MESSAGE_BYTE);
            if (data != null) {
                jsonStr = PushUtil.decodeMessage(data, secret);
            } else {
                jsonStr = bundle.getString(KEY_PUSH_MESSAGE_STRING);
            }

            if (Util.isStringValid(jsonStr)) {
                if (jsonStr.startsWith("http://") || jsonStr.startsWith("https://")) {
                    PushLog.i("Receive short link message!");
                    String deviceId = DroiPushBridge.generateUUID(mContext);
                    getShortLinkMessage(mContext, appKey, deviceId, msgId, jsonStr, secret);
                } else {
                    PushMsg pmsg = new PushMsg(appKey, jsonStr);
                    pmsg.setPackageName(packageName);
                    if (pmsg.isControlMessage()) {
                        parseContorlMsg(pmsg);
                    } else if (pmsg.valid) {
                        PushUtil.savePushMessageIfNecessary(mContext, pmsg);
                        loadPushMessage(pmsg);
                    }
                }
            }
        }
    }

    public interface RunnableCallback {
        public void onRunnableResult(boolean isSuccess, JSONObject data);
    }

    public void parseContorlMsg(PushMsg msg) {
        if (msg == null || !msg.isControlMessage()) {
            return;
        }

        int cType = msg.cmd_type;
        String cData = msg.cmd_data;
        switch (cType) {
        case PushMsg.CMD_TYPE_HEARTBEAT_SETTING:
            PushSetting.getInstance(mContext).processHeartBeat(cData);
            int newHeartbeat = PushSetting.getInstance(mContext).getHeartBeat();
            if (DroiPush.myUdpClient != null) {
                DroiPush.myUdpClient.setHeartbeatInterval(newHeartbeat);
            }
            break;

        case PushMsg.CMD_TYPE_FORCE_OFFLINE:
            PushSetting.getInstance(mContext).saveRecoveryTime(msg.packageName, msg.app_key, cData);
            break;

        case PushMsg.CMD_TYPE_SILENT_SETTING:
            PushSetting.getInstance(mContext).processSlientTime(msg);
            break;

        case PushMsg.CMD_TYPE_UPDATE_ADDRESS:
            getServerIpAddress(mContext);
            break;

        default:
            break;
        }
    }

    private String getAppSecret(String appKey) {
        if (mAppInfo != null) {
            Pair<String, String> info = mAppInfo.get(appKey);
            if (info != null) {
                return info.first;
            }
        }
        return null;
    }

    private String getAppPackage(String appKey) {
        if (mAppInfo != null) {
            Pair<String, String> info = mAppInfo.get(appKey);
            if (info != null) {
                return info.second;
            }
        }
        return null;
    }

    private void addAppInfo(String appKey, String secret, String packageName) {
        if (Util.isStringValid(appKey) && Util.isStringValid(secret) && Util.isStringValid(packageName)) {
            Pair<String, String> info = new Pair<String, String>(secret, packageName);
            if (mAppInfo != null && !mAppInfo.containsKey(appKey)) {
                mAppInfo.put(appKey, info);
            }
        }
    }
}
