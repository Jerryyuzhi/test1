package com.droi.sdk.push;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;
import com.droi.sdk.DroiException;
import com.droi.sdk.push.utils.Base64Util;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import com.droi.sdk.utility.BitmapBackgroundCallback;
import com.droi.sdk.utility.Utility;
import android.graphics.Bitmap;

public class PushMsg {

    // message timing type
    public static final int MSG_TIMING_NONE = 1;
    public static final int MSG_TIMING_ONESHOT = 2;
    public static final int MSG_TIMING_CYCLIC = 3;

    // message style
    public static final int MSGTYPE_INVALID = -1;
    public static final int MSGTYPE_SIMPLE = 1;
    public static final int MSGTYPE_IMAGE = 2;
    public static final int MSGTYPE_CUSTOM = 4;
    public static final int MSGTYPE_CONTROL = 5;

    // action type of normal message
    public static final int MSG_ACTION_OPENACTIVITY = 1;
    public static final int MSG_ACTION_OPENBROWSER = 2;
    public static final int MSG_ACTION_DOWNLOADAPP = 3;

    // action type of control message
    public static final int CMD_TYPE_SILENT_SETTING = 1;
    public static final int CMD_TYPE_HEARTBEAT_SETTING = 2;
    public static final int CMD_TYPE_FORCE_OFFLINE = 3;
    public static final int CMD_TYPE_UPDATE_ADDRESS = 4;

    // keywords for json keys
    static final String KEY_MSG_ID = "mi";
    static final String KEY_MSG_TYPE = "ms";
    static final String KEY_APP_KEY = "mk";
    static final String KEY_NOTIFI_TITLE = "mt";
    static final String KEY_NOTIFI_CONTENT = "mb";
    static final String KEY_NOTIFI_TICKER = "r";
    static final String KEY_NOTIFI_ICON = "sc";
    static final String KEY_NOTIFI_BIG_ICON = "sb";
    static final String KEY_MSG_SHOW_INTERVAL = "si";
    static final String KEY_MSG_SHOW_TIME = "st";
    static final String KEY_NOTIFI_LIGHT = "mf";
    static final String KEY_NOTIFI_CLEAR = "mc";
    static final String KEY_NOTIFI_VIBRATE = "mv";
    static final String KEY_NOTIFI_RING = "mr";
    static final String KEY_MSG_ACTION_TYPE = "at";
    static final String KEY_MSG_ACTION_DATA = "ad";
    static final String KEY_MSG_EXTRA_DATA = "ed";
    static final String KEY_MSG_ACTION_ICON = "ai";
    static final String KEY_MSG_CMD_TYPE = "ct";
    static final String KEY_MSG_CMD_DATA = "cd";

    public long msg_id;// mi
    public int msg_type;// ms
    public String app_key;// mk
    public String notify_icon;// sc
    public String big_icon;// sb
    public long interval;// si
    public long time_1;
    public long time_2;
    public String action_icon;// ai

    // control message type:
    // 1.setting silent time
    // 2.set heart beat interval
    // 3.force offline
    public int cmd_type = -1;// ct

    // Control message parameter:
    // ct = 1: silent time
    // ct = 2: heart beat interval(seconds)
    // ct = 3: restore time
    public String cmd_data = null;// cd

    public String mMsgJson = null;

    public boolean valid = false;
    public String tag;

    public String packageName = null;

    public Set<String> mImagesSet = new HashSet<String>();
    public Set<String> mDownloadedSet = new HashSet<String>();
    private AtomicInteger mDownloadedNum = new AtomicInteger(0);
    private int mTotalPicNum;
    private int mMsgTimingType = MSG_TIMING_NONE;

    private ImageLoadListener mListener;

    public PushMsg(String appKey, String jsonStr) {
        this(jsonStr);
        this.app_key = appKey;
    }

    public PushMsg(String jsonStr) {
        mMsgJson = jsonStr;
        valid = true;// initial value is true
        JSONObject jsonObj = null;

        try {
            jsonObj = new JSONObject(jsonStr);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }

        if (jsonObj != null) {
            msg_id = parseMsgId(jsonObj);
            msg_type = parseMsgType(jsonObj);
            app_key = parseAppKey(jsonObj);
            if (msg_type != MSGTYPE_CONTROL) {
                notify_icon = parseNotifiIcon(jsonObj);
                if (Util.isStringValid(notify_icon)) {
                    mImagesSet.add(notify_icon);
                }

                big_icon = parseBigImage(jsonObj);
                if (Util.isStringValid(big_icon)) {
                    mImagesSet.add(big_icon);
                }

                interval = parseInterval(jsonObj) * 60000L;

                long timingBounds[] = parseTimingBounds(jsonObj);
                if (timingBounds.length == 2) {
                    time_1 = timingBounds[0];
                    time_2 = timingBounds[1];
                } else {
                    time_1 = 0;
                    time_2 = 0;
                }

                if (interval < 0) {
                    valid = false;
                } else if (interval > 0) {
                    mMsgTimingType = MSG_TIMING_CYCLIC;
                } else if (interval == 0) {
                    if (time_2 > time_1) {
                        mMsgTimingType = MSG_TIMING_ONESHOT;
                        long curTime = System.currentTimeMillis();
                        if (curTime > time_2) {
                            this.valid = false;
                        }
                    } else if (time_2 < time_1) {
                        this.valid = false;
                    } else {
                        this.valid = (time_1 == 0);
                    }
                }

                action_icon = parseActionIcon(jsonObj);
                if (Util.isStringValid(action_icon)) {
                    mImagesSet.add(action_icon);
                }
            } else {
                cmd_type = parseCommandType(jsonObj);
                cmd_data = parseCommandData(jsonObj);
            }

            String prefix = "T";
            if (isTimingMessage() || isCyclicMessage()) {
                prefix = "P";
            }
            tag = prefix + System.currentTimeMillis() + msg_id;

            mTotalPicNum = mImagesSet.size();
        }
    }

    public PushMsg(long msgId, String appKey, String tag, String content, long interval, long time1, long time2) {
        this.msg_id = msgId;
        this.app_key = appKey;
        this.tag = tag;
        this.interval = interval;
        this.time_1 = time1;
        this.time_2 = time2;
        this.mMsgJson = Base64Util.DecodeFromBase64Data(content);
        this.valid = true;

        if (interval < 0) {
            valid = false;
        } else if (interval > 0) {
            mMsgTimingType = MSG_TIMING_CYCLIC;
        } else if (interval == 0) {
            if (time_2 > time_1) {
                mMsgTimingType = MSG_TIMING_ONESHOT;
                long curTime = System.currentTimeMillis();
                if (curTime > time_2) {
                    this.valid = false;
                }
            } else if (time2 < time1) {
                this.valid = false;
            } else {
                this.valid = (time1 == 0);
            }
        }

        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(mMsgJson);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }

        msg_type = parseMsgType(jsonObj);

        notify_icon = parseNotifiIcon(jsonObj);
        if (Util.isStringValid(notify_icon)) {
            mImagesSet.add(notify_icon);
        }

        big_icon = parseBigImage(jsonObj);
        if (Util.isStringValid(big_icon)) {
            mImagesSet.add(big_icon);
        }

        action_icon = parseActionIcon(jsonObj);
        if (Util.isStringValid(action_icon)) {
            mImagesSet.add(action_icon);
        }

        mTotalPicNum = mImagesSet.size();
    }

    public void startDownloadResource() {
        if (mImagesSet == null || mImagesSet.isEmpty()) {
            return;
        }
        for (String path : mImagesSet) {
            final String p = path;
            try {
                Utility.getBitmapInBackground(p, 0, 0, new BitmapBackgroundCallback() {

                    @Override
                    public void result(boolean success, Bitmap bitmap) {
                        // TODO Auto-generated method stub
                        onDownloadResult(p, success);
                    }
                });
            } catch (DroiException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                onDownloadResult(p, false);
            }
        }
    }

    public interface ImageLoadListener {
        public void onImageLoadResult(PushMsg msg, boolean result);
    }

    public void setImageLoadListener(ImageLoadListener listener) {
        mListener = listener;
    }

    public synchronized void onDownloadResult(String path, boolean isSuccess) {
        if (isSuccess) {
            boolean exist = mDownloadedSet.contains(path);
            if (!exist) {
                mDownloadedNum.addAndGet(1);
                mDownloadedSet.add(path);
            }
            if (mDownloadedNum.get() == mTotalPicNum) {
                PushLog.i("PushMsg_" + msg_id + ": images all loaded");
                mListener.onImageLoadResult(this, true);
            }
        } else {
            mListener.onImageLoadResult(this, false);
        }
    }

    public String encodePushMsg() {
        return Base64Util.EncodeIntoBase64(mMsgJson);
    }

    public boolean isDisposableMessage() {
        return mMsgTimingType == MSG_TIMING_NONE;
    }

    public boolean isTimingMessage() {
        return mMsgTimingType == MSG_TIMING_ONESHOT;
    }

    public boolean isCyclicMessage() {
        return mMsgTimingType == MSG_TIMING_CYCLIC;
    }

    public boolean isControlMessage() {
        return msg_type == MSGTYPE_CONTROL;
    }

    public boolean needDownloadImage() {
        return !mImagesSet.isEmpty();
    }

    public String getMsgIdentifier() {
        return tag;
    }

    public String getTagIfCyclicMessage() {
        String value = null;
        if (isCyclicMessage()) {
            value = tag;
        }
        return value;
    }

    public void setPackageName(String name) {
        this.packageName = name;
    }

    public boolean shouldShowMessageNow() {
        boolean value = false;
        long curTime = System.currentTimeMillis();

        if (mMsgTimingType == MSG_TIMING_ONESHOT) {
            if (curTime >= time_1 && curTime <= time_2) {
                value = true;
            }
        } else if (mMsgTimingType == MSG_TIMING_CYCLIC) {
            if (Math.abs(curTime - time_1) >= interval) {
                value = true;
            }
        } else {
            value = true;
        }

        return value;
    }

    public boolean isValid() {
        return valid;
    }

    public boolean showCyclicMessageNow() {
        if (valid && isCyclicMessage()) {
            long curTime = System.currentTimeMillis();
            if (Math.abs(curTime - time_1) >= interval) {
                return true;
            }
        }
        return false;
    }

    public boolean showTimingMessageNow() {
        if (valid && isTimingMessage()) {
            long curTime = System.currentTimeMillis();
            if (curTime > time_1 && curTime < time_2) {
                return true;
            } else if (curTime > time_2) {
                valid = false;
            }
        }
        return false;
    }

    static long parseMsgId(JSONObject jsonObj) {
        long result = -1;
        if (jsonObj != null) {
            try {
                result = jsonObj.getLong(KEY_MSG_ID);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static int parseMsgType(JSONObject jsonObj) {
        int result = MSGTYPE_INVALID;
        if (jsonObj != null) {
            try {
                result = jsonObj.getInt(KEY_MSG_TYPE);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseAppKey(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_APP_KEY);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseTitle(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_NOTIFI_TITLE);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseContent(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_NOTIFI_CONTENT);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseTicker(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_NOTIFI_TICKER);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseNotifiIcon(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_NOTIFI_ICON);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseBigImage(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_NOTIFI_BIG_ICON);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static long parseInterval(JSONObject jsonObj) {
        long result = 0;
        if (jsonObj != null) {
            try {
                result = jsonObj.getLong(KEY_MSG_SHOW_INTERVAL);
                result = (result > 0) ? result : 0;
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseTiming(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_MSG_SHOW_TIME);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static boolean parseFlashLight(JSONObject jsonObj) {
        boolean result = true;
        if (jsonObj != null) {
            try {
                result = jsonObj.getBoolean(KEY_NOTIFI_LIGHT);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static boolean parseClearable(JSONObject jsonObj) {
        boolean result = true;
        if (jsonObj != null) {
            try {
                result = jsonObj.getBoolean(KEY_NOTIFI_CLEAR);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static boolean parseVibrate(JSONObject jsonObj) {
        boolean result = true;
        if (jsonObj != null) {
            try {
                result = jsonObj.getBoolean(KEY_NOTIFI_VIBRATE);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static boolean parseRing(JSONObject jsonObj) {
        boolean result = true;
        if (jsonObj != null) {
            try {
                result = jsonObj.getBoolean(KEY_NOTIFI_RING);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static int parseActionType(JSONObject jsonObj) {
        int result = -1;
        if (jsonObj != null) {
            try {
                result = jsonObj.getInt(KEY_MSG_ACTION_TYPE);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseActionData(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_MSG_ACTION_DATA);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseExtraActionData(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_MSG_EXTRA_DATA);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseActionIcon(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_MSG_ACTION_ICON);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static int parseCommandType(JSONObject jsonObj) {
        int result = -1;
        if (jsonObj != null) {
            try {
                result = jsonObj.getInt(KEY_MSG_CMD_TYPE);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    static String parseCommandData(JSONObject jsonObj) {
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_MSG_CMD_DATA);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }
        return result;
    }

    private long[] parseTimingBounds(JSONObject jsonObj) {
        long[] timeBounds = new long[] { 0, 0 };
        String result = null;
        if (jsonObj != null) {
            try {
                result = jsonObj.getString(KEY_MSG_SHOW_TIME);
            } catch (JSONException e) {
                PushLog.e(e);
            }
        }

        if (Util.isStringValid(result)) {
            int dividerPos = result.indexOf("-");
            if (dividerPos > 0) {
                String time1 = result.substring(0, dividerPos);
                String time2 = result.substring(dividerPos + 1, result.length());
                timeBounds[0] = Util.getUTCTime(time1);
                timeBounds[1] = Util.getUTCTime(time2);
            }
        }
        return timeBounds;
    }
}