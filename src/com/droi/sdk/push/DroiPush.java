package com.droi.sdk.push;

import java.util.concurrent.ExecutorService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.droi.sdk.DroiException;
import com.droi.sdk.analytics.priv.AnalyticsModule;
import com.droi.sdk.push.core.UDPClientBase;
import com.droi.sdk.push.core.WapClientBase;
import com.droi.sdk.push.data.DatabaseManager;
import com.droi.sdk.push.utils.DroiPushBridge;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;

public class DroiPush {
    private static DroiPush sDroiPush = null;
    private Context mContext = null;
    private DroiPushHandler mMessageHandler = null;
    private int mCurNoticeId = 0;
    private NotificationManager mNotifyMgr;
    private PushSetting mSetting;

    static DatabaseManager mManager;
    static Looper mLooper = null;
    static ExecutorService mPool;
    static UDPClientBase myUdpClient;
    static WapClientBase myWapClient;
    public static AnalyticsModule mAnalyticsCollector;

    Handler mMainThreadHandler = new Handler(Looper.getMainLooper());

    private DroiPush(final Context ctx) {
        mContext = ctx.getApplicationContext();
        mAnalyticsCollector = new AnalyticsModule(mContext);
        mNotifyMgr = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mSetting = PushSetting.getInstance(ctx);
        mMessageHandler = new DroiPushHandler() {
            @Override
            public void onCustomMessage(Context context, DroiPushNotification notice) {
                PushLog.v("Wrong onCustomMessage");
            }
        };

        saveAccountInfo(ctx, mSetting);

        String mode = Util.getTransferMode(mContext);
        Intent startSrv = new Intent(Constants.INTENT_PUSHSERVICE);
        String appkey = Util.getAppKey(ctx);
        String secret = Util.getSecret(ctx);
        String packageName = ctx.getPackageName();
        startSrv.putExtra(DroiPushService.KEY_APP_KEY, appkey);
        startSrv.putExtra(DroiPushService.KEY_SECRET, secret);
        startSrv.putExtra(DroiPushService.KEY_PACKAGE, packageName);
        if (mode != null && mode.equals(Constants.TRANSFER_MODE_TCP)) {
            startSrv.putExtra(Constants.KEY_CMD, Constants.KEY_RESET_TCP);
        } else {
            startSrv.putExtra(Constants.KEY_CMD, Constants.KEY_RESET_UDP);
        }

        PushUtil.startPushService(mContext, startSrv);
    }

    /**
     * Initialize the push SDK
     * 
     * @param context
     */
    public static void initialize(Context context) {
        synchronized (DroiPush.class) {
            if (sDroiPush == null) {
                sDroiPush = new DroiPush(context);
            }
        }
    }

    /**
     * Get the instance of DroiPush. DroiPush must be initialized firstly,
     * otherwise DroiException would be thrown out
     * 
     * @return DroiPush instance
     * @throws DroiException
     *             Exception would be thrown if DroiPush was not initialized
     *             firstly
     */
    public static DroiPush getInstance() throws DroiException {
        synchronized (DroiPush.class) {
            if (sDroiPush == null) {
                throw new DroiException("DroiPush object was uninitialized");
            }
        }
        return sDroiPush;
    }

    /**
     * Add tag for the application
     * 
     * @param tag
     *            A string describing the application
     * @return true if adding is successful, false otherwise
     */
    public boolean addTag(String tag) {
        return mSetting.addTag(tag);
    }

    /**
     * Add tags for the application
     * 
     * @param tags
     *            A set of string which describes the application
     * @return true if adding is successful, false otherwise
     */
    public boolean addTag(String[] tags) {
        return mSetting.addTags(tags);
    }

    /**
     * Remove the tag of the application
     * 
     * @param tag
     *            A string which describes the application
     * @return true if removing is successful, false otherwise
     */
    public boolean removeTag(String tag) {
        return mSetting.removeTag(tag);
    }

    /**
     * Remove the tags of the application
     * 
     * @param tags
     *            A set of string which describes the application
     * @return true if removing is successful, false otherwise
     */
    public boolean removeTag(String[] tags) {
        return mSetting.removeTags(tags);
    }

    /**
     * Get all of the tags
     * 
     * @return A string that contains all the tags which are separated by a
     *         comma.
     */
    public String getTags() {
        return mSetting.getTags();
    }

    /**
     * Setting silent time for the application
     * 
     * @param startHour
     *            Hour part of starting time
     * @param startMin
     *            Minute part of starting time
     * @param endHour
     *            Hour part of end time
     * @param endMin
     *            Minute part of end time
     * @return true if setting is successful, false otherwise
     */
    public boolean setSilentTime(int startHour, int startMin, int endHour, int endMin) {
        String packageName = mContext.getPackageName();
        return mSetting.saveSilentTime(packageName, false, startHour, startMin, endHour, endMin);
    }

    /**
     * Get the silent time of the push service
     * 
     * @return A array to save start hour, start minute, end hour and end minute
     *         by order
     */
    public int[] getSilentTime() {
        String packageName = mContext.getPackageName();
        return mSetting.getAppSettingSilentTime(packageName);
    }

    /**
     * Enable or disable the push service in client
     * 
     * @param isEnable
     *            true if enable push service, otherwise disable
     */
    public void setPushEnabled(Boolean isEnable) {
        mSetting.setPushEnabled(isEnable);
    }

    /**
     * Get the setting of push service in client
     * 
     * @return true if the service is enabled, otherwise is disabled
     */
    public boolean getIsPushEnabled() {
        return mSetting.getIsPushEnabled();
    }

    /**
     * Set user custom message handler which can handle message according to
     * user's will
     * 
     * @param handler
     *            Instance of 'DroiPushHandler' or subclass
     */
    public void setMessageHandler(DroiPushHandler handler) {
        mMessageHandler = handler;
    }

    void notifyExt(final DroiPushNotification notice) {
        String deviceId = DroiPushBridge.generateUUID(mContext);
        String header = Util.buildAnalyticsHeader();
        String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, notice.msg_id, Constants.ANALYTICS_RECEIVE_MSG, deviceId);
        Util.sendAnalyticsInfo(mAnalyticsCollector, header, content);

        mMainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                mCurNoticeId = (++mCurNoticeId) % Integer.MAX_VALUE;
                notice.setNoticeId(mCurNoticeId);
                showNotification(notice);
            }
        });
    }

    private void showNotification(DroiPushNotification notice) {
        if(notice == null){
            return;
        }
        switch (notice.msg_type) {
        case PushMsg.MSGTYPE_CUSTOM:
            PushLog.v("Custom Msg");
            mMessageHandler.onCustomMessage(mContext, notice);
            break;

        default:
            if (mMessageHandler != null) {
                Notification noti = mMessageHandler.getNotification(mContext, notice);
                if (noti != null) {
                    // send analytics info
                    String deviceId = DroiPushBridge.generateUUID(mContext);
                    String header = Util.buildAnalyticsHeader();
                    String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, notice.msg_id,
                            Constants.ANALYTICS_SHOW_NOTIFICATION, deviceId);
                    Util.sendAnalyticsInfo(mAnalyticsCollector, header, content);

                    // Builds the notification and issues it.
                    mNotifyMgr.notify(notice.getNoticeTag(), notice.getNoticeId(), noti);
                }
            }
            break;
        }
    }

    void onNotificationClicked(long msgId, String tag, int id) {
        if (mNotifyMgr != null) {
            if (msgId > 0) {
                //send analytics info
                String deviceId = DroiPushBridge.generateUUID(mContext);
                String header = Util.buildAnalyticsHeader();
                String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, msgId, Constants.ANALYTICS_DELETE_NOTIFICATION, deviceId);
                Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
            }
            mNotifyMgr.cancel(tag, id);
        }
    }

    void saveAccountInfo(Context ctx, PushSetting setting) {
        SharedPreferences account = ctx.getSharedPreferences(Constants.DEFAULT_PRE_NAME, Context.MODE_PRIVATE);
        boolean appRegStatus = account.getBoolean(Constants.REG_APPINFO_STATUS, false);
        boolean tagsRegStatus = setting.hasTagsChanged();
        // Upload the app info if the tags changed;
        if (!appRegStatus || tagsRegStatus) {
            PushUtil.uploadAppInfo(ctx);
        }
        boolean devRegStatus = account.getBoolean(Constants.REG_DEVINFO_STATUS, false);
        if (!devRegStatus) {
            PushUtil.uploadDeviceInfo(ctx);
        }
    }

    static void clearPushEnvironment() {
        if (myUdpClient != null) {
            try {
                myUdpClient.stop();
                myUdpClient = null;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        if (mLooper != null) {
            mLooper.quit();
            mLooper = null;
        }

        if (mPool != null) {
            mPool.shutdownNow();
            mPool = null;
        }

        if (mManager != null) {
            mManager.onDestroy();
            mManager = null;
        }
    }
}
