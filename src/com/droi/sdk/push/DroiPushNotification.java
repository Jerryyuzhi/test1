package com.droi.sdk.push;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import android.content.Context;

public class DroiPushNotification implements Serializable {
    private static final long serialVersionUID = -2974340729174692235L;

    // notification id
    private int noticeId;
    // notification tag
    private String tag = null;

    /**
     * Message id
     */
    public final long msg_id;

    /**
     * Message type: 
     * msg_type = 1 textual message
     * msg_type = 2 pure picture message
     * msg_type = 4 transmitted message
     * msg_type = 5 control message
     */
    public final int msg_type;// ms

    /**
     * App key
     */
    public final String appKey;// mk

    /**
     * Title of the notification
     */
    public final String title;// mt

    /**
     * Content of the notification
     */
    public final String content;// mb

    /**
     * Ticker of the notification
     */
    public final String ticker;// r

    /**
     * Url of the image:
     * msg_type = 1, 3 Url of the small icon in notification
     * msg_type = 2 Url of the whole image notification
     */
    public final String notify_icon;// sc

    /**
     * Url of the image shown in expand notification
     */
    public final String big_icon;// sb

    /**
     * Whether to display flash
     */
    public final boolean flashlight;// mf

    /**
     * Whether the notification can be deleted
     */
    public final boolean clear;// mc

    /**
     * Whether to display vibrate
     */
    public final boolean vibrate;// mv

    /**
     * Whether to display ringtone
     */
    public final boolean ring;// mr

    /**
     * The action of clicking notification:
     * action_type = 1 run application
     * action_type = 2 open URL
     * action_type = 3 download APK
     */
    public final int action_type;// at

    /**
     * The data used in click action:
     * action_type = 1 the name of the activity to be launched
     * action_type = 2 the URL to be loaded
     * action_type = 3 the URL of the APK to be downloaded
     */
    public final String action_data;// ad

    /**
     * The extra data used in click action:
     * action_type = 1 the data passed to activity
     * action_type = 2 useless
     * action_type = 3 the package name of the APK
     */
    public final String action_extra_data;// ed

    /**
     * The URL of the image shown after click action
     */
    public final String action_icon;// ai

    /**
     * Type of the command message
     */
    int cmd_type = -1;

    /**
     * The data part of command message
     */
    String cmd_data = null;

    public DroiPushNotification(String tag, String jsonStr) {

        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(jsonStr);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }

        msg_id = PushMsg.parseMsgId(jsonObj);
        msg_type = PushMsg.parseMsgType(jsonObj);
        appKey = PushMsg.parseAppKey(jsonObj);
        title = PushMsg.parseTitle(jsonObj);
        content = PushMsg.parseContent(jsonObj);
        ticker = PushMsg.parseTicker(jsonObj);
        notify_icon = PushMsg.parseNotifiIcon(jsonObj);
        big_icon = PushMsg.parseBigImage(jsonObj);
        flashlight = PushMsg.parseFlashLight(jsonObj);
        clear = PushMsg.parseClearable(jsonObj);
        vibrate = PushMsg.parseVibrate(jsonObj);
        ring = PushMsg.parseRing(jsonObj);
        action_type = PushMsg.parseActionType(jsonObj);
        action_data = PushMsg.parseActionData(jsonObj);
        action_extra_data = PushMsg.parseExtraActionData(jsonObj);
        action_icon = PushMsg.parseActionIcon(jsonObj);
        cmd_type = PushMsg.parseCommandType(jsonObj);
        cmd_data = PushMsg.parseCommandData(jsonObj);

        this.tag = tag;
    }

    /**
     * Get the id of the notification
     * 
     * @return id of the notification
     */
    public int getNoticeId() {
        return this.noticeId;
    }

    /**
     * Get the tag of the notification
     * 
     * @return tag of the notification
     */
    public String getNoticeTag() {
        return tag;
    }

    boolean isSimpleNotification() {
        return (msg_type == PushMsg.MSGTYPE_SIMPLE);
    }

    boolean isImageNotification() {
        return (msg_type == PushMsg.MSGTYPE_IMAGE);
    }

    void setNoticeId(int id) {
        if (tag == null) {
            this.noticeId = id;
        }
    }

    boolean isNoticeGetDisplayed(Context context) {
        if (isSimpleNotification()) {
            if (!Util.isStringValid(title) || !Util.isStringValid(content)) {
                return false;
            }
        }
        if (action_type == PushMsg.MSG_ACTION_DOWNLOADAPP) {
            if (!Util.isStringValid(action_extra_data)) {
                return false;
            }
            String[] names = action_extra_data.split("\\|");
            String pkgName = names[0];

            if (!Util.isStringValid(pkgName) || Util.isAppInstalled(context, pkgName)) {
                return false;
            }
        }
        return true;
    }
}