package com.droi.sdk.push.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PushSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    public PushSQLiteOpenHelper(Context context) {
        super(context, DatabaseColumns.PUSH_DB_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        CreateDatabase(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    private void CreateDatabase(SQLiteDatabase db) {
        // create appinfo table
        db.execSQL("create table if not exists " + DatabaseColumns.APPINFO_TAB_NAME + " (" + DatabaseColumns.ID
                + " integer primary key autoincrement, " + DatabaseColumns.APPKEY + " text, " + DatabaseColumns.SECRET + " text, " +DatabaseColumns.PACKAGE + " text); ");

        // create message table
        db.execSQL("create table if not exists " + DatabaseColumns.MESSAGE_TAB_NAME + " (" + DatabaseColumns.ID
                + " integer primary key autoincrement, " + DatabaseColumns.MSG_ID + " integer, " + DatabaseColumns.APPKEY + " text, "
                + DatabaseColumns.TAG + " text, " + DatabaseColumns.CONTENT + " text, " + DatabaseColumns.INTERVAL
                + " integer, " + DatabaseColumns.TIME_1 + " integer, " + DatabaseColumns.TIME_2 + " integer);");

        // create history table
        db.execSQL("create table if not exists " + DatabaseColumns.HISTORY_TAB_NAME + " (" + DatabaseColumns.ID
                + " integer primary key autoincrement, " + DatabaseColumns.MSG_ID + " integer, " + DatabaseColumns.APPKEY + " text, "
                + DatabaseColumns.CREATETIME + " integer);");

        // create sdk info table
        db.execSQL("create table if not exists " + DatabaseColumns.SDKINFO_TAB_NAME + " (" + DatabaseColumns.ID
                + " integer primary key autoincrement, " + DatabaseColumns.REORGINAZE_TIME + " integer);");
    }
}
