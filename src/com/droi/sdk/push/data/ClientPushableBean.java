package com.droi.sdk.push.data;

import java.io.Serializable;

public class ClientPushableBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean pushable;

    public ClientPushableBean(boolean pushable) {
        super();
        this.pushable = pushable;
    }

    public boolean isPushable() {
        return this.pushable;
    }
}
