package com.droi.sdk.push.data;

import android.provider.BaseColumns;

public class DatabaseColumns implements BaseColumns {
    public static final String PUSH_DB_NAME = "push.db";
    public static final String MESSAGE_TAB_NAME = "message";
    public static final String APPINFO_TAB_NAME = "appinfo";
    public static final String HISTORY_TAB_NAME = "history";
    public static final String SDKINFO_TAB_NAME = "sdk";

    // message appInfo table
    public static final String ID = "_id";
    public static final String APPKEY = "appkey";
    public static final String SECRET = "secret";
    public static final String PACKAGE = "package";

    // message table
    public static final String MSG_ID = "msg_id";
    public static final String TAG = "tag";
    public static final String CONTENT = "content";
    public static final String INTERVAL = "interval";
    public static final String TIME_1 = "time_1";
    public static final String TIME_2 = "time_2";

    // history time
    public static final String CREATETIME = "createtime";

    // sdk info
    public static final String REORGINAZE_TIME = "reorgtime";

    public static final String[] MESSAGE_ALL_COLUMNS = new String[] { MSG_ID, APPKEY, TAG, CONTENT, INTERVAL, TIME_1,
            TIME_2};
    public static final String[] APPINFO_ALL_COLUMNS = new String[] { MSG_ID, APPKEY, SECRET, PACKAGE };
}
