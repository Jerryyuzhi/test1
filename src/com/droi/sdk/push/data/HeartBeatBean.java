package com.droi.sdk.push.data;

import java.io.Serializable;

public class HeartBeatBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private int time;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public HeartBeatBean(int time) {
        super();
        this.time = time;
    }

}
