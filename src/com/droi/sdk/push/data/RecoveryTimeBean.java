package com.droi.sdk.push.data;

import java.io.Serializable;

public class RecoveryTimeBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private long startingTime;
    private long interval;

    public RecoveryTimeBean(long interval) {
        super();
        startingTime = System.currentTimeMillis();
        this.interval = interval;
    }

    public boolean isRecovered() {
        if (this.interval > 0) {
            long curTime = System.currentTimeMillis();
            return (Math.abs(curTime - startingTime) > this.interval);
        }
        return true;
    }
}
