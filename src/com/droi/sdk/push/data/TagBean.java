package com.droi.sdk.push.data;

import java.io.Serializable;

public class TagBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String[] tags;
    private boolean hasChanged = false;

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public boolean isHasChanged() {
        return hasChanged;
    }

    public void setHasChanged(boolean hasChanged) {
        this.hasChanged = hasChanged;
    }

    public TagBean(String[] tags) {
        super();
        this.tags = tags;
    }

    @Override
    public String toString() {
        if (tags == null || tags.length == 0) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < tags.length; i++) {
            if (i == 0) {
                buffer.append(tags[0].trim());
            } else {
                buffer.append("," + tags[i].trim());
            }
        }
        return buffer.toString();
    }

}
