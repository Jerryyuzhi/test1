package com.droi.sdk.push.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Pair;

import com.droi.sdk.push.PushMsg;
import com.droi.sdk.push.utils.FilePathUtil;
import com.droi.sdk.push.utils.Util;

public class DatabaseManager {

    private static DatabaseManager mInstance;
    private PushSQLiteOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;
    private Context mContext;

    public static DatabaseManager getInstance(Context context) {
        synchronized (DatabaseManager.class) {
            if (mInstance == null) {
                mInstance = new DatabaseManager(context);
            }
        }
        return mInstance;
    }

    public DatabaseManager(Context context) {
        mContext = context;

        if (Util.isExistSDCard()) {
            mDatabaseHelper = new PushSQLiteOpenHelper(new ExternalDatabaseContext(mContext.getApplicationContext()));
        } else {
            mDatabaseHelper = new PushSQLiteOpenHelper(mContext);
        }

        try {
            mDatabase = mDatabaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public boolean savePushMsg(PushMsg msg) {
        if (msg == null) {
            return false;
        }
        try {
            if (mDatabase == null || !mDatabase.isOpen()) {
                return false;
            }
            ContentValues value = new ContentValues();
            value.put(DatabaseColumns.MSG_ID, msg.msg_id);
            value.put(DatabaseColumns.APPKEY, msg.app_key);
            value.put(DatabaseColumns.TAG, msg.tag);
            value.put(DatabaseColumns.CONTENT, msg.encodePushMsg());
            value.put(DatabaseColumns.INTERVAL, msg.interval);
            value.put(DatabaseColumns.TIME_1, msg.time_1);
            value.put(DatabaseColumns.TIME_2, msg.time_2);
            mDatabase.insert(DatabaseColumns.MESSAGE_TAB_NAME, null, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            handleDBFileNotFoundException();
            return false;
        }
    }

    public List<PushMsg> getLocalStoredMessage() {
        List<PushMsg> mList = null;
        try {
            if (mDatabase != null) {
                Cursor cursor = mDatabase.query(DatabaseColumns.MESSAGE_TAB_NAME, DatabaseColumns.MESSAGE_ALL_COLUMNS,
                        null, null, null, null, null);
                if (cursor != null && cursor.getCount() != 0) {
                    mList = new ArrayList<PushMsg>();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        long id = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseColumns.MSG_ID));
                        String appKey = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseColumns.APPKEY));
                        String tag = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseColumns.TAG));
                        String content = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseColumns.CONTENT));
                        long interval = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseColumns.INTERVAL));
                        long time_1 = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseColumns.TIME_1));
                        long time_2 = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseColumns.TIME_2));
                        PushMsg pmsg = new PushMsg(id, appKey, tag, content, interval, time_1, time_2);
                        mList.add(pmsg);
                        cursor.moveToNext();
                    }
                    cursor.close();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
        return mList;
    }

    public HashMap<String, Pair<String, String>> getAppInfo() {
        HashMap<String, Pair<String, String>> appInfo = new HashMap<String, Pair<String, String>>();
        try {
            if (mDatabase != null) {
                Cursor cursor = mDatabase.query(DatabaseColumns.APPINFO_TAB_NAME, DatabaseColumns.APPINFO_ALL_COLUMNS,
                        null, null, null, null, null);
                if (cursor != null && cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        String appKey = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseColumns.APPKEY));
                        String secret = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseColumns.SECRET));
                        String packageName = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseColumns.SECRET));
                        Pair<String, String> sp = new Pair<String, String>(secret, packageName);
                        appInfo.put(appKey, sp);
                        cursor.moveToFirst();
                    }
                    cursor.close();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
        return appInfo;
    }

    public void deleteMessageByTag(String tag) {
        try {
            if (mDatabase != null && tag != null) {
                mDatabase.delete(DatabaseColumns.MESSAGE_TAB_NAME, DatabaseColumns.TAG + " like '" + tag + "'", null);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
    }

    public void updateLastShowTime(String tag, long lastTime) {
        try {
            if (mDatabase != null) {
                String where = DatabaseColumns.TAG + " like '" + tag + "'";
                ContentValues cv = new ContentValues();
                cv.put(DatabaseColumns.TIME_1, lastTime);
                mDatabase.update(DatabaseColumns.MESSAGE_TAB_NAME, cv, where, null);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
    }

    public void insertAppInfo(String appkey, String secret, String packageName) {
        try {
            if (mDatabase != null) {
                mDatabase.delete(DatabaseColumns.APPINFO_TAB_NAME, DatabaseColumns.APPKEY + " like '" + appkey + "'",
                        null);
                ContentValues cv = new ContentValues();
                cv.put(DatabaseColumns.APPKEY, appkey);
                cv.put(DatabaseColumns.SECRET, secret);
                cv.put(DatabaseColumns.PACKAGE, packageName);
                mDatabase.insert(DatabaseColumns.APPINFO_TAB_NAME, null, cv);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
    }

    public boolean isMessageOverdue(long interval, long time_1, long time_2) {
        boolean value = true;
        long curTime = System.currentTimeMillis();
        if (interval > 0) {
            if (time_1 > 0 && curTime > time_1 && curTime < time_2) {
                value = false;
            }
        }
        return value;
    }

    private void handleDBFileNotFoundException() {
        if (Util.isExistSDCard()) {
            String dbPath = FilePathUtil.getSDCardCachePath() + "/" + DatabaseColumns.PUSH_DB_NAME;
            File dbFile = new File(dbPath);
            if (!dbFile.exists()) {
                mDatabaseHelper.close();
                mDatabaseHelper = null;
                mDatabaseHelper = new PushSQLiteOpenHelper(
                        new ExternalDatabaseContext(mContext.getApplicationContext()));
                try {
                    mDatabase = mDatabaseHelper.getWritableDatabase();
                } catch (SQLiteException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        } else {
            mDatabaseHelper.close();
            mDatabaseHelper = null;
            mDatabaseHelper = new PushSQLiteOpenHelper(mContext);
            try {
                mDatabase = mDatabaseHelper.getWritableDatabase();
            } catch (SQLiteException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }

    public Cursor getPushHistoryCursor() {
        Cursor c = null;
        try {
            String[] columns = new String[] { DatabaseColumns.APPKEY, DatabaseColumns.MSG_ID,
                    DatabaseColumns.CREATETIME };
            if (mDatabase != null) {
                c = mDatabase.query(DatabaseColumns.HISTORY_TAB_NAME, columns, null, null, null, null, null);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
        return c;
    }

    public void deletePushRecord(String appKey, long msgId) {
        String where = DatabaseColumns.APPKEY + " like '" + appKey + "' AND " + DatabaseColumns.MSG_ID + "=" + msgId;
        if (mDatabase != null) {
            mDatabase.delete(DatabaseColumns.HISTORY_TAB_NAME, where, null);
        }
    }

    public void insertPushRecord(long msgId, String appKey, long createtime) {
        try {
            if (mDatabase != null) {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseColumns.MSG_ID, msgId);
                cv.put(DatabaseColumns.APPKEY, appKey);
                cv.put(DatabaseColumns.CREATETIME, createtime);
                mDatabase.insert(DatabaseColumns.HISTORY_TAB_NAME, null, cv);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
    }

    public long getLastOrginazeTime() {
        long value = 0;
        try {
            String[] columns = new String[] { DatabaseColumns.REORGINAZE_TIME };
            if (mDatabase != null) {
                Cursor c = mDatabase.query(DatabaseColumns.SDKINFO_TAB_NAME, columns, null, null, null, null, null);
                if (c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    value = c.getLong(c.getColumnIndexOrThrow(DatabaseColumns.REORGINAZE_TIME));
                    c.close();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
        return value;
    }

    public void saveLastReorganizeTime(long time) {
        try {
            if (mDatabase != null) {
                mDatabase.delete(DatabaseColumns.SDKINFO_TAB_NAME, null, null);
                ContentValues cv = new ContentValues();
                cv.put(DatabaseColumns.REORGINAZE_TIME, time);
                mDatabase.insert(DatabaseColumns.SDKINFO_TAB_NAME, null, cv);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            handleDBFileNotFoundException();
        }
    }

    public void onDestroy() {
        if (mDatabase != null) {
            mDatabase.close();
            mDatabase = null;
        }

        if (mDatabaseHelper != null) {
            mDatabaseHelper.close();
            mDatabaseHelper = null;
        }
    }
}