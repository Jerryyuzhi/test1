package com.droi.sdk.push.data;

import java.io.Serializable;

public class SlienceTimeBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private int startHour = -1;
    private int startMin = -1;
    private int endHour = -1;
    private int endMin = -1;

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMin() {
        return startMin;
    }

    public void setStartMin(int startMin) {
        this.startMin = startMin;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getEndMin() {
        return endMin;
    }

    public void setEndMin(int endMin) {
        this.endMin = endMin;
    }

    public SlienceTimeBean(int startHour, int startMin, int endHour, int endMin) {
        super();
        this.startHour = startHour;
        this.startMin = startMin;
        this.endHour = endHour;
        this.endMin = endMin;
    }

}
