package com.droi.sdk.push.data;

import java.io.Serializable;

public class ServiceIpsBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String[] httpAddress;
    private String[] udpAddress;

    public ServiceIpsBean(String[] udpAddress, String[] httpAddress) {
        super();
        this.udpAddress = udpAddress;
        this.httpAddress = httpAddress;
    }

    public String[] getUdpAddress() {
        return udpAddress;
    }

    public String[] getHttpAddress() {
        return httpAddress;
    }
}
