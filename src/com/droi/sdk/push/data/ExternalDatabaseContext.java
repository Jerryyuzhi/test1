package com.droi.sdk.push.data;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.os.Environment;

import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.FilePathUtil;

public class ExternalDatabaseContext extends ContextWrapper {
    public ExternalDatabaseContext(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public boolean isSDCardAvaliable() {
        boolean sdCardExist = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        return sdCardExist;
    }

    public File getDatabaseFile() {
        if (!isSDCardAvaliable()) {
            return null;
        }

        String path = FilePathUtil.getSDCardCachePath();

        File rootFolder = new File(path);
        if (!rootFolder.exists()) {
            rootFolder.mkdirs();
        }

        boolean createSuccess = false;

        String dbPath = path + "/" + DatabaseColumns.PUSH_DB_NAME;
        File dbFile = new File(dbPath);

        if (!dbFile.exists()) {
            try {
                createSuccess = dbFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                PushLog.e(e);
            }
        }

        createSuccess = true;

        if (createSuccess) {
            return dbFile;
        } else {
            return null;
        }
    }

    @Override
    public SQLiteDatabase openOrCreateDatabase(String name, int mode, CursorFactory factory,
            DatabaseErrorHandler errorHandler) {
        // TODO Auto-generated method stub
        File dbFile = getDatabaseFile();
        SQLiteDatabase database = null;

        if (dbFile != null) {
            database = SQLiteDatabase.openOrCreateDatabase(getDatabaseFile(), null);
        }

        return database;
    }

    @Override
    public SQLiteDatabase openOrCreateDatabase(String name, int mode, CursorFactory factory) {
        // TODO Auto-generated method stub
        File dbFile = getDatabaseFile();
        SQLiteDatabase database = null;

        if (dbFile != null) {
            database = SQLiteDatabase.openOrCreateDatabase(getDatabaseFile(), null);
        }

        return database;
    }
}