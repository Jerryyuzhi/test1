/*
 *Copyright 2014 DDPush
 *Author: AndyKwok(in English) GuoZhengzhu(in Chinese)
 *Email: ddpush@126.com
 *

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package com.droi.sdk.push.core;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;
import com.droi.sdk.push.Constants;
import com.droi.sdk.push.data.ServiceIpsBean;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.StringUtil;
import com.droi.sdk.push.utils.Util;

public abstract class UDPClientBase implements Runnable {

    protected DatagramSocket ds;
    protected long lastSent = 0;
    protected long lastReceived = 0;
    protected byte[] uuid;

    protected ConcurrentLinkedQueue<Message> mq = new ConcurrentLinkedQueue<Message>();

    protected AtomicLong queueIn = new AtomicLong(0);
    protected AtomicLong queueOut = new AtomicLong(0);

    protected int bufferSize = 1024;
    protected int heartbeatInterval = 50;

    protected byte[] bufferArray;
    protected ByteBuffer buffer;
    protected boolean needReset = true;

    protected boolean started = false;
    protected boolean stoped = false;

    protected Thread receiverT;
    protected Worker worker;
    protected Thread workerT;

    private String[] mServerUdpAddress;

    public UDPClientBase(byte[] uuid, int appid) throws Exception {
        if (uuid == null || uuid.length != 16) {
            throw new java.lang.IllegalArgumentException("uuid byte array must be not null and length of 16 bytes");
        }
        if (appid < 1 || appid > 255) {
            throw new java.lang.IllegalArgumentException("appid must be from 1 to 255");
        }
        this.uuid = uuid;
        loadServerUdpAddress();
    }

    public void loadServerUdpAddress() {
        ServiceIpsBean ipBean = null;
        String[] tempUdp = null;
        try {
            ipBean = (ServiceIpsBean) Util.getSerializableBySD(Util.md5(Constants.DROI_SERVER_IP));
        } catch (Exception e) {
        }

        if (ipBean != null) {
            tempUdp = ipBean.getUdpAddress();
        }

        if (tempUdp != null) {
            mServerUdpAddress = new String[tempUdp.length + 1];
            System.arraycopy(tempUdp, 0, mServerUdpAddress, 0, tempUdp.length);
            mServerUdpAddress[tempUdp.length] = Constants.DEFAULT_UDP_SERVER;
        } else {
            mServerUdpAddress = new String[1];
            mServerUdpAddress[0] = Constants.DEFAULT_UDP_SERVER;
        }
    }

    protected boolean enqueue(Message message) {
        boolean result = mq.add(message);
        if (result == true) {
            queueIn.addAndGet(1);
        }
        return result;
    }

    protected Message dequeue() {
        Message m = mq.poll();
        if (m != null) {
            queueOut.addAndGet(1);
        }
        return m;
    }

    private synchronized void init() {
        bufferArray = new byte[bufferSize];
        buffer = ByteBuffer.wrap(bufferArray);
    }

    protected synchronized void reset() throws Exception {
        if (needReset == false) {
            return;
        }

        if (ds != null) {
            try {
                ds.close();
            } catch (Exception e) {
            }
        }

        if (isNetworkAvaliable()) {
            ds = new DatagramSocket();
            String[] udpAddress = mServerUdpAddress;
            int len = udpAddress.length;
            for (int i = 0; i < len; i++) {
                try {
                    ds.connect(buildSocketAddress(udpAddress[i]));
                    needReset = false;
                    break;
                } catch (SocketException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    private InetSocketAddress buildSocketAddress(String address) {
        InetSocketAddress result = null;
        String host = null;
        String port = null;

        if (Util.isStringValid(address)) {
            String addressPart[] = address.split(":");
            if (addressPart.length == 2) {
                host = addressPart[0];
                port = addressPart[1];
                try {
                    result = new InetSocketAddress(host, Integer.parseInt(port));
                } catch (NumberFormatException e) {
                    // TODO: handle exception
                    result = null;
                }
            }
        }
        return result;
    }

    public synchronized void start() throws Exception {
        if (this.started == true) {
            return;
        }
        this.init();

        receiverT = new Thread(this, "udp-client-receiver");
        receiverT.setDaemon(true);
        synchronized (receiverT) {
            receiverT.start();
            receiverT.wait();
        }

        worker = new Worker();
        workerT = new Thread(worker, "udp-client-worker");
        workerT.setDaemon(true);
        synchronized (workerT) {
            workerT.start();
            workerT.wait();
        }

        this.started = true;
    }

    public void stop() {
        stoped = true;
        if (ds != null) {
            try {
                ds.close();
            } catch (Exception e) {
            }
            ds = null;
        }
        if (receiverT != null) {
            try {
                receiverT.interrupt();
            } catch (Exception e) {
            }
        }

        if (workerT != null) {
            try {
                workerT.interrupt();
            } catch (Exception e) {
            }
        }
    }

    public void run() {
        synchronized (receiverT) {
            receiverT.notifyAll();
        }

        while (stoped == false) {
            try {
                if (!isNetworkAvaliable()) {
                    try {
                        trySystemSleep();
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                    continue;
                }
                reset();
                heartbeat();
                receiveData();
            } catch (java.net.SocketTimeoutException e) {

            } catch (Exception e) {
                PushLog.e(e);
                this.needReset = true;
            } catch (Throwable t) {
                t.printStackTrace();
                this.needReset = true;
            } finally {
                if(needReset || mq.isEmpty() || !isNetworkAvaliable()) {
                    try {
                        trySystemSleep();
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                }
            }
        }
        if (ds != null) {
            try {
                ds.close();
            } catch (Exception e) {
            }
            ds = null;
        }
    }

    private void heartbeat() throws Exception {
        if (Math.abs(System.currentTimeMillis() - lastSent) < heartbeatInterval * 1000) {
            return;
        }
        byte[] prefix = new byte[] { 0x44, 0x52 };// 'DR'
        byte[] buffer = new byte[Message.CLIENT_MESSAGE_MIN_LENGTH];
        ByteBuffer.wrap(buffer).put(prefix).put((byte) Message.version).put((byte) Message.CMD_0x00).put(uuid)
                .putLong((long) 0).putChar((char) 0);
        send(buffer);
    }

    private void receiveData() throws Exception {
        DatagramPacket dp = new DatagramPacket(bufferArray, bufferArray.length);
        ds.setSoTimeout(5 * 1000);
        ds.receive(dp);
        if (dp.getLength() <= 0 || dp.getData() == null || dp.getData().length == 0) {
            return;
        }

        byte[] data = new byte[dp.getLength()];
        System.arraycopy(dp.getData(), 0, data, 0, dp.getLength());

        Message m = new Message(dp.getSocketAddress(), data);
        PushLog.v("KEY:" + m.getAppKey());
        PushLog.v("Len:" + m.getContentLength());
        PushLog.v("CMD:" + m.getCmd());
        if (m.checkFormat() == false) {
            return;
        }
        this.lastReceived = System.currentTimeMillis();
        this.ackServer(m);
        if (m.getCmd() == Message.CMD_0x00) {
            return;
        }

        if (!isMessageExist(m)) {
            this.enqueue(m);
            worker.wakeup();
        }
    }

    private void ackServer(Message m) throws Exception {
        byte[] prefix = new byte[] { 0x44, 0x52 };// 'DR'
        if (m.getCmd() == Message.CMD_0x10) {
            byte[] buffer = new byte[Message.CLIENT_MESSAGE_MIN_LENGTH];
            ByteBuffer.wrap(buffer).put(prefix).put((byte) Message.version).put((byte) Message.CMD_0x10).put(uuid)
                    .putChar((char) 0).putChar((char) 0);
            send(buffer);
        }
        if (m.getCmd() == Message.CMD_0x11) {
            byte[] buffer = new byte[Message.CLIENT_MESSAGE_MIN_LENGTH + 8];
            byte[] data = m.getData();
            ByteBuffer.wrap(buffer).put(prefix).put((byte) Message.version).put((byte) Message.CMD_0x11).put(uuid)
                    .putChar((char) 0).putChar((char) 8).put(data, Message.SERVER_MESSAGE_MIN_LENGTH, 8);
            send(buffer);
        }
        if (m.getCmd() == Message.CMD_0x20) {
            long msgId = m.getMsgId();
            byte[] buffer = new byte[Message.CLIENT_MESSAGE_MIN_LENGTH];
            ByteBuffer.wrap(buffer).put(prefix).put((byte) Message.version).put((byte) Message.CMD_0x20).put(uuid)
                    .putLong(msgId).putChar((char) 0);
            send(buffer);
        }
        if (m.getCmd() == Message.CMD_0x40) {
            byte[] buffer = new byte[Message.CLIENT_MESSAGE_MIN_LENGTH];
            ByteBuffer.wrap(buffer).put(prefix).put((byte) Message.version).put((byte) Message.CMD_0x40).put(uuid)
                    .putChar((char) 0).putChar((char) 0);
            send(buffer);
        }
    }

    private void send(byte[] data) throws Exception {
        if (data == null) {
            return;
        }

        PushLog.v("Before Send: " + StringUtil.convert(data));

        if (ds == null) {
            return;
        }

        DatagramPacket dp = new DatagramPacket(data, data.length);
        dp.setSocketAddress(ds.getRemoteSocketAddress());
        ds.send(dp);
        lastSent = System.currentTimeMillis();
    }

    public void setBufferSize(int bytes) {
        this.bufferSize = bytes;
    }

    public int getBufferSize() {
        return this.bufferSize;
    }

    public long getLastHeartbeatTime() {
        return lastSent;
    }

    public long getLastReceivedTime() {
        return lastReceived;
    }

    /*
     * send heart beat every given seconds
     */
    public void setHeartbeatInterval(int second) {
        if (second <= 0) {
            return;
        }
        PushLog.i("set heartbeat===" + second);
        this.heartbeatInterval = second;
    }

    public int getHeartbeatInterval() {
        return this.heartbeatInterval;
    }

    public abstract boolean isMessageExist(Message msg);

    public abstract boolean isNetworkAvaliable();

    public abstract void trySystemSleep();

    public abstract void onPushMessage(Message message);

    class Worker implements Runnable {
        public void run() {
            synchronized (workerT) {
                workerT.notifyAll();
            }
            while (stoped == false) {
                try {
                    handleEvent();
                } catch (Exception e) {
                    PushLog.e(e);
                } finally {
                    waitMsg();
                }
            }
        }

        private void waitMsg() {
            synchronized (this) {
                try {
                    this.wait(1000);
                } catch (java.lang.InterruptedException e) {

                } catch (Exception e) {
                    PushLog.e(e);
                }
            }
        }

        private void wakeup() {
            synchronized (this) {
                this.notifyAll();
            }
        }

        private void handleEvent() throws Exception {
            Message m = null;
            while (true) {
                m = dequeue();
                if (m == null) {
                    return;
                }
                if (m.checkFormat() == false) {
                    continue;
                }

                // real work here
                onPushMessage(m);
            }
            // finish work here, such as release wake lock
        }

    }
}
