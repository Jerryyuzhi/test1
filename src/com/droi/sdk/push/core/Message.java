/*
 *Copyright 2014 DDPush
 *Author: AndyKwok(in English) GuoZhengzhu(in Chinese)
 *Email: ddpush@126.com
 *

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package com.droi.sdk.push.core;

import java.net.SocketAddress;
import java.nio.ByteBuffer;

import com.droi.sdk.push.utils.StringUtil;

public final class Message {

    public static int version = 1;
    public static final int SERVER_MESSAGE_MIN_LENGTH = 8;
    public static final int CLIENT_MESSAGE_MIN_LENGTH = 30;
    public static final int CMD_0x00 = 0x00;// heart beat packet
    public static final int CMD_0x10 = 0x10;// general message
    public static final int CMD_0x11 = 0x11;// category message
    public static final int CMD_0x20 = 0x20;// custom message
    public static final int CMD_0x40 = 0x40;

    protected SocketAddress address;
    protected byte[] data;

    public Message(SocketAddress address, byte[] data) {
        this.address = address;
        this.data = data;
    }

    public int getContentLength() {
        int cmd = getCmd();
        if (cmd == CMD_0x20) {
            return (int) ByteBuffer.wrap(data, SERVER_MESSAGE_MIN_LENGTH + 20, 2).getChar();
        } else {
            return (int) ByteBuffer.wrap(data, SERVER_MESSAGE_MIN_LENGTH - 2, 2).getChar();
        }
    }

    public byte[] getContent() {
        int cmd = getCmd();
        int dataLen = getContentLength();
        ByteBuffer bb;

        if (cmd == CMD_0x20) {
            bb = ByteBuffer.wrap(data, SERVER_MESSAGE_MIN_LENGTH + 22, dataLen);
        } else {
            bb = ByteBuffer.wrap(data, SERVER_MESSAGE_MIN_LENGTH, dataLen);
        }
        byte[] ret = new byte[bb.remaining()];
        bb.get(ret);
        return ret;
    }

    public String getAppKey() {
        int cmd = getCmd();
        if (cmd == CMD_0x20) {
            return StringUtil.convert(data, 4, 16);
        } else {
            return null;
        }
    }

    public int getCmd() {
        byte b = data[3];
        return b & 0xff;
    }

    public boolean checkFormat() {
        if (address == null || data == null || data.length < Message.SERVER_MESSAGE_MIN_LENGTH) {
            return false;
        }
        int cmd = getCmd();
        if (cmd != CMD_0x00 && cmd != CMD_0x10 && cmd != CMD_0x11 && cmd != CMD_0x20 && cmd != CMD_0x40) {
            return false;
        }
        int dataLen = getContentLength();
        if (cmd == CMD_0x20) {
            if (data.length < dataLen + SERVER_MESSAGE_MIN_LENGTH + 22) {
                return false;
            }
        } else {
            if (data.length < dataLen + SERVER_MESSAGE_MIN_LENGTH) {
                return false;
            }
        }
        if (cmd == CMD_0x10 && dataLen != 0) {
            return false;
        }

        if (cmd == CMD_0x11 && dataLen != 8) {
            return false;
        }

        if (cmd == CMD_0x20 && dataLen < 1) {// must has content
            return false;
        }
        return true;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return this.data;
    }

    public void setSocketAddress(SocketAddress address) {
        this.address = address;
    }

    public SocketAddress getSocketAddress() {
        return this.address;
    }

    public static void setVersion(int v) {
        if (v < 1 || v > 255) {
            return;
        }
        version = v;
    }

    public static int getVersion() {
        return version;
    }

    public long getMsgId() {
        long msgId = ByteBuffer.wrap(data, SERVER_MESSAGE_MIN_LENGTH + 12, 8).getLong();
        return msgId;
    }
}
