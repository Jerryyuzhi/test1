package com.droi.sdk.push.core;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.droi.sdk.push.Constants;
import com.droi.sdk.push.PushMsg;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import android.content.Context;

public abstract class WapClientBase implements Runnable {
    private final int REQUEST_MESSAGE_INTERVAL_LONG = 300000;
    private final int REQUEST_MESSAGE_INTERVAL_SHORT = 60000;

    private String mDeviceId;
    private Context mContext;
    protected boolean started = false;
    protected boolean stopped = false;
    protected Thread mThread;

    public WapClientBase(Context context, String deviceId) {
        mContext = context;
        mDeviceId = deviceId;
    }

    private String getSendData() {
        String sendData = null;
        JSONObject obj = new JSONObject();
        try {
            obj.put(Constants.KEY_OS_TYPE, Constants.OSTYPE);
            obj.put(Constants.KEY_TIME_STAMP, System.currentTimeMillis() + "");
            obj.put(Constants.KEY_SDK_VERSION, Constants.SDK_VERSION);
            obj.put(Constants.KEY_DEVICE_ID, mDeviceId);
            sendData = obj.toString();
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return sendData;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (!stopped) {
            try {
                if (isNetworkAvailable()) {
                    acquireAndDispatchMessage();
                    Thread.sleep(REQUEST_MESSAGE_INTERVAL_LONG);
                } else {
                    Thread.sleep(REQUEST_MESSAGE_INTERVAL_SHORT);
                }
            } catch (Exception e) {
                PushLog.e(e);
            }
        }
    }

    /**
     * Request and dispatch push message in wap network
     */
    private void acquireAndDispatchMessage() {
        JSONObject retJson = null;
        try {
            String sendData = getSendData();
            if (sendData == null) {
                PushLog.e("Create sending data failed!");
                return;
            }
            String secret = Util.getSecret(mContext);
            String appKey = Util.getAppKey(mContext);
            String ret = Util.postData(mContext.getApplicationContext(), Constants.PUSH_CMWAP_SERVER_SUFFIX, sendData,
                    secret, appKey);
            if (ret != null) {
                retJson = new JSONObject(ret);
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            PushLog.e(e1);
            e1.printStackTrace();
        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            PushLog.e("Parse json error and return, reason: " + e2);
        }

        String result = null;
        JSONObject data = null;
        boolean success = false;
        if (retJson != null) {
            try {
                result = retJson.getString(Constants.KEY_RESULT);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                PushLog.e("Parse json error and return, reason: " + e);
                return;
            }

            try {
                data = retJson.getJSONObject(Constants.KEY_DATA);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                PushLog.e("Parse json error and return, reason: " + e);
                return;
            }
        }

        if (result == null || data == null) {
            PushLog.w("The data or result received in cmwap mode is null!");
            return;
        }

        success = result.equals(Constants.KEY_RESPONSE_SUCCESS) ? true : false;
        if (success) {
            JSONArray msgArray = null;
            try {
                msgArray = data.getJSONArray(Constants.KEY_MESSAGE_LIST);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                PushLog.e("Parse json error and return, reason: " + e);
                return;
            }

            if (msgArray != null) {
                for (int i = 0; i < msgArray.length(); i++) {
                    String msg = null;
                    try {
                        msg = msgArray.getString(i);
                        if (Util.isStringValid(msg)) {
                            PushMsg pmsg = new PushMsg(msg);
                            onPushMessage(pmsg);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } else {
            String errCode = null;
            try {
                errCode = data.getString(Constants.KEY_ERROR_CODE);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                PushLog.e(e);
            }
            PushLog.w("Get push message in cmwap mode failed, errorCode: " + errCode);
        }
    }

    public synchronized void start() {
        if (started == true) {
            return;
        }
        mThread = new Thread(this, "wap-client");
        mThread.setDaemon(true);
        mThread.start();
        started = true;
    }

    public abstract boolean isMessageExist(PushMsg msg);

    public abstract boolean isNetworkAvailable();

    public abstract void trySystemSleep();

    public abstract void onPushMessage(PushMsg msg);

    public void stop() {
        stopped = true;
    }
}