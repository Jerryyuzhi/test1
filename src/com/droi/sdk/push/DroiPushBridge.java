package com.droi.sdk.push;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Pair;

import com.droi.sdk.core.PushCoreHelper;
import com.droi.sdk.push.Constants;
import com.droi.sdk.push.PushSetting;
import com.droi.sdk.push.utils.DESUtil;
import com.droi.sdk.push.utils.DeviceInfo;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;

public class DroiPushBridge {
    private static final String KEY_RESPONOSE_CODE = "rCode";

    public static String generateUUID(Context ctx) {
        String uuid = null;
        uuid = PushCoreHelper.getDeviceId(ctx);
        if (uuid != null) {
            return uuid.replace("-", "");
        }
        return uuid;
    }

    static void uploadAppInfo(final Context ctx) {
        final String appKey = Util.getAppKey(ctx);
        final String secret = Util.getSecret(ctx);
        final PushSetting setter = PushSetting.getInstance(ctx);
        final String tags = setter.getTags();
        if (appKey != null) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    String ret;
                    try {
                        String sendData = Util.getAppInfo(ctx, tags);
                        PushLog.v("SendAppInfo: " + sendData);
                        ret = Util.postData(ctx, Constants.APP_INFO_UPLOAD_SERVER, sendData, secret, appKey);

                        PushLog.v("AppInfo:" + ret);
                        JSONObject jsonobj = null;
                        jsonobj = new JSONObject(ret);
                        int rcode = jsonobj.getInt(KEY_RESPONOSE_CODE);
                        if (rcode == 200) {
                            SharedPreferences account = ctx.getSharedPreferences(Constants.DEFAULT_PRE_NAME,
                                    Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = account.edit();
                            editor.putBoolean(Constants.REG_APPINFO_STATUS, true);
                            editor.commit();
                            setter.setTagsUploaded();
                            PushLog.v("Upload AppInfo Success");
                        }
                    } catch (IOException e) {
                        PushLog.e(e);
                    } catch (JSONException e) {
                        PushLog.e(e);
                    }
                }
            }).start();
        }
        return;
    }

    static void uploadDeviceInfo(final Context context) {
        final String info = DeviceInfo.get(context);
        final String appKey = Util.getAppKey(context);
        final String secret = Util.getSecret(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                String ret;
                try {
                    PushLog.v(info);
                    ret = Util.postData(context, Constants.DEV_INFO_UPLOAD_SERVER, info, secret, appKey);
                    PushLog.v("DevInfo:" + ret);
                    JSONObject jsonobj = null;
                    jsonobj = new JSONObject(ret);
                    int rcode = jsonobj.getInt(KEY_RESPONOSE_CODE);
                    if (rcode == 200) {
                        SharedPreferences account = context.getSharedPreferences(Constants.DEFAULT_PRE_NAME,
                                Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = account.edit();
                        editor.putBoolean(Constants.REG_DEVINFO_STATUS, true);
                        editor.commit();
                        PushLog.v("Upload DevInfo Success");
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    PushLog.e(e);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    PushLog.e(e);
                }
            }
        }).start();
    }

    public static byte[] encryptData(String key, byte[] data) {
        if (data == null) {
            return null;
        }
        try {
            return DESUtil.encrypt(data, key.getBytes());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
            return null;
        }
    }

    public static byte[] decryptData(String key, byte[] data) {
        try {
            return DESUtil.decrypt(data, key.getBytes());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            PushLog.v("Decrypt data error, Check App Key and Secret");
            PushLog.e(e);
            return null;
        }
    }

    public static void onNotificationClick(int type) {
        PushLog.v("Clicked on type:" + type);
        return;
    }

    static void openPushService(Context context, Intent intent) {
        Pair<String, String> runningService = Util.getRunningPushService(context);
        if (runningService != null) {
            String curPackageName = context.getApplicationContext().getPackageName();
            // running service is not the current, clear and stop current
            // service now
            if (!curPackageName.equals(runningService.first)) {
                // clear and stop current service
                DroiPush.clearPushEnvironment();
                Intent stopSrv = new Intent();
                stopSrv.setAction(Constants.INTENT_PUSHSERVICE);
                stopSrv.setPackage(context.getPackageName());
                context.stopService(stopSrv);

                // passed intent to running service
                Intent startSrv = new Intent(intent);
                ComponentName cn = new ComponentName(runningService.first, runningService.second);
                startSrv.setComponent(cn);
                context.startService(startSrv);
            } else {// current service is running
                    // current service is the latest?
                Pair<String, String> latestService = Util.getLatestServiceInfo(context);
                if (latestService != null && !latestService.first.equals(runningService.first)) {
                    // clear and stop current service
                    DroiPush.clearPushEnvironment();
                    Intent stopSrv = new Intent();
                    stopSrv.setAction(Constants.INTENT_PUSHSERVICE);
                    stopSrv.setPackage(context.getPackageName());
                    context.stopService(stopSrv);

                    // set up the latest
                    if (intent != null) {
                        Intent startSrv = new Intent(intent);
                        ComponentName cn = new ComponentName(latestService.first, latestService.second);
                        startSrv.setComponent(cn);
                        context.startService(startSrv);
                    }
                } else {// current service is the latest service
                    if (intent != null) {
                        Intent startSrv = new Intent(intent);
                        startSrv.setAction(Constants.INTENT_PUSHSERVICE);
                        startSrv.setPackage(context.getPackageName());
                        context.startService(startSrv);// pass intent
                    }
                }
            }
        } else {// no service is running now, set up the latest service
            if (intent != null) {
                Pair<String, String> latestService = Util.getLatestServiceInfo(context);
                Intent startSrv = new Intent(intent);
                ComponentName cn = new ComponentName(latestService.first, latestService.second);
                startSrv.setComponent(cn);
                context.startService(startSrv);
            }
        }
    }
    
    static String decodeMessage(byte[] data, String secret) {
        byte[] decryptedData = DroiPushBridge.decryptData(secret, data);
        if (decryptedData == null) {
            return null;
        }
        // notice for padding
        int i;
        for (i = decryptedData.length - 1; i >= 0; i--) {
            if (decryptedData[i] != 0) {
                break;
            }
        }

        byte[] decryptedDataNoZero = new byte[i + 1];
        System.arraycopy(decryptedData, 0, decryptedDataNoZero, 0, i + 1);

        String decoded = null;
        try {
            decoded = new String(decryptedDataNoZero, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }

        return decoded;
    }
}
