package com.droi.sdk.push;

public class Constants {

    // SDK parameter
    public static final byte OSTYPE = 1;
    public static final int SDK_VERSION = 1;
    public static final String ANALYTICS_TYPE_PUSH = "3";

    public static String DEFAULT_PRE_NAME = "defaultAccount";
    public static String REG_APPINFO_STATUS = "regAppInfoStatus";
    public static String REG_DEVINFO_STATUS = "regDevInfoStatus";
    public static String REG_TAG_INFO = "tagInfo";

    // The name of the files which saved the data in control message 
    public static final String DROI_SERVER_HB = "droiServerHeartBeat";
    public static final String DROI_DEFAULT_HB = "droiDefaultHeartBeat";
    public static final String DROI_SERVER_RT = "droiServerRecoveryTime";
    public static final String DROI_CLIENT_PH = "droiClientPushable";
    public static final String DROI_SERVER_ST = "droiServerSlienceTime";
    public static final String DROI_CLIENT_ST = "droiUserSlienceTime";
    public static final String DROI_SERVER_IP = "droiServerIpList";
    public static final String DROI_TAG_FILE_NAME = "droiUserTags.txt";

    // The directory of databases 
    public static final String SDCARD_DRODPUSH_DIR = "/.droipush/";
    public static final String SDCARD_RESCACHE_DIR = "/db/";

    public static final String INTENT_PUSHSERVICE = "com.droi.sdk.push.action.START";
    public static final String INTENT_PUSHDATA = "com.droi.sdk.push.action.DATA";
    public static final String INTENT_SHOW_APP_DETAIL = "com.droi.sdk.push.action.SHOW_DETAIL";

    // The default and the suffix part of server address
    public static final String APP_INFO_UPLOAD_SERVER = "http://data-droipush.tt286.com:2400/data/appInfo";
    public static final String DEV_INFO_UPLOAD_SERVER = "http://data-droipush.tt286.com:2400/data/deviceInfo";
    public static final String DEFAULT_UDP_SERVER = "211.151.211.249:2100";
    public static final String DEFAULT_HTTP_SERVER = "211.151.211.249:2500";
    public static final String MSG_VALIDITY_VERIFY_SERVER_SUFFIX = "/device/message/status";
    public static final String PUSH_CMWAP_SERVER_SUFFIX = "/device/message/polling";
    public static final String PUSH_RANDOM_IP_SERVER_SUFFIX = "/device/ip";
    public static final String KEY_ACTION_SUFFIX = ".Action.START";

    // The action in push service
    public static final String KEY_CMD = "CMD";
    static final String KEY_NETWORK_CHANGE = "NETWORK_CHANGE";
    static final String KEY_TICK = "TICK";
    static final String KEY_HEART_BEAT = "HEART_BEAT";
    static final String KEY_RESET_TCP = "RESET_TCP";
    static final String KEY_RESET_UDP = "RESET_UDP";

    // The keys of the app configuration
    public static final String KEY_CONFIG_APPKEY = "DROI_APPKEY";
    public static final String KEY_CONFIG_SECRET = "PUSH_MESSAGE_SECRET";
    public static final String KEY_CONFIG_CHANNEL = "DROI_CHANNEL";
    public static final String DEFAULT_CHANNEL = "Droi";
    public static final String DEFAULT_SECRET = "new_push";
    public static final String KEY_TRANSFER_MODE = "Mode";
    public static final String TRANSFER_MODE_TCP = "TCP";
    public static final String TRANSFER_MODE_UDP = "UDP";

    //The keys in json which would be uploaded 
    public static final String KEY_OS_TYPE = "osType";
    public static final String KEY_TIME_STAMP = "timeStamp";
    public static final String KEY_SDK_VERSION = "sdkVer";
    public static final String KEY_DEVICE_ID = "deviceId";
    public static final String KEY_MESSAGE_ID = "msgId";
    public static final String KEY_APP_VERSION_CODE = "appVer";
    public static final String KEY_APP_VERSION_NAME = "appBuild";
    public static final String KEY_CHANNEL_ID = "channelId";
    public static final String KEY_APP_TAGS = "tags";
    public static final String KEY_MSG_ID = "msgId";
    
    //The keywords of response from server
    public static final String KEY_RESULT = "result";
    public static final String KEY_DATA = "data";
    public static final String KEY_ERROR_CODE = "errorCode";
    public static final String KEY_RESPONSE_SUCCESS = "success";
    public static final String KEY_MESSAGE_LIST = "msgList";
    public static final String KEY_MESSAGE_INFO = "msgInfo";
    public static final String KEY_ADDRESS_LIST = "ipList";
    public static final String KEY_MODULE_ID = "moduleId";
    public static final String KEY_HOST = "host";
    public static final String KEY_PORT = "port";
    public static final int MODULE_HTTP = 2;
    public static final int MODULE_UDP = 1;
    public static final int DEFAULT_HEARTBEAT_INTERVAL = 100;
    public static final int MAX_HEARTBEAT_INTERVAL = 180;

    //analytics action
    public static final String ANALYTICS_TYPE_01 = "m01";
    public static final int ANALYTICS_RECEIVE_MSG = 0;
    public static final int ANALYTICS_SHOW_NOTIFICATION = 1;
    public static final int ANALYTICS_CLICK_NOTIFICATION = 2;
    public static final int ANALYTICS_DELETE_NOTIFICATION = 3;
    public static final int ANALYTICS_SHOW_APP_DETAIL = 4;
    public static final int ANALYTICS_CLICK_DOWNLOAD_BTN = 5;
    public static final int ANALYTICS_CLICK_CANCEL_BTN = 6;
    public static final int ANALYTICS_DOWNLOAD_SUCCESS = 7;
    public static final int ANALYTICS_SHOW_INSTALL_WINDOW = 8;
    public static final int ANALYTICS_INSTALL_SUCCESS = 9;

    // The default log file name
    public static final String SDCARD_LOG_FILE_NAME = "DroiLog.txt";

    // The switch to enable or disable the debug
    public static final boolean DEBUG_LOG_FILE_ON = false;

    public static final String LEVEL_FILE_NAME = "DroiLogLevel.dat";

    // The duration of every push record
    public static final long PUSH_RECORD_KEEP_TIME_MILLISECOND = 2592000000L;
    public static final long DEF_TIME_PERIOD = 600000L;
}
