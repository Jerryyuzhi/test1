package com.droi.sdk.push;

import com.droi.sdk.DroiException;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import com.droi.sdk.utility.Utility;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;

public abstract class DroiPushHandler {

    // Keywords of notification layout
    public static final String KEY_ID = "id";
    public static final String KEY_LAYOUT = "layout";
    public static final String KEY_PUSH_NOTIFI_LAYOUT = "dp_push_notification_layout";
    public static final String KEY_SIMPLE_NOTIFI_PART = "dp_simple_notification_part";
    public static final String KEY_IMAGE_NOTIFI_PART = "dp_image_notification_part";
    public static final String KEY_BIG_IMAGE_NOTIFI_LAYOUT = "dp_big_image_notification_layout";

    public static final String KEY_NORMAL_IMAGEVIEW_ID = "dp_normal_image";
    public static final String KEY_BIG_IMAGEVIEW_ID = "dp_big_image";
    public static final String KEY_NOTIFI_ICON_ID = "dp_notify_icon";
    public static final String KEY_NOTIFI_TITLE_ID = "dp_notify_title";
    public static final String KEY_NOTIFI_CONTENT_ID = "dp_notify_text";

    // Keywords of notification tag and id
    public static final String KEY_NOTIFI_TAG = "tag";
    public static final String KEY_NOTIFI_ID = "id";

    // Keywords of the data in notification
    public static final String KEY_ACTION_TYPE = "type";
    public static final String KEY_ACTIVITY_NAME = "activity";
    public static final String KEY_ACTIVITY_DATA = "data";
    public static final String KEY_ACTIVITY_PASS_DATA = "push_data";
    public static final String KEY_MSG_ID = "msgid";

    public static final String KEY_WEB_URL = "url";

    public static final String KEY_PACKAGE_NAME = "package";
    public static final String KEY_DOWNLOAD_URL = "durl";
    public static final String KEY_ILLUSTRATION_URL = "iurl";
    public static final String KEY_DOWNLOAD_INFO = "dinfo";

    public abstract void onCustomMessage(Context context, DroiPushNotification notice);

    public Notification getNotification(Context context, DroiPushNotification notice) {
        Notification notification = null;
        switch (notice.msg_type) {
        case PushMsg.MSGTYPE_IMAGE:
            notification = buildImageNotification(context, notice);
            break;

        case PushMsg.MSGTYPE_SIMPLE:
            notification = buildSimpleNotification(context, notice);
            break;

        default:
            notification = null;
            break;
        }

        return notification;
    }

    @TargetApi(16)
    private Notification buildSimpleNotification(Context context, DroiPushNotification notice) {
        Notification notification = null;
        if (!notice.isNoticeGetDisplayed(context)) {
            return null;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        builder.setContentTitle(notice.title);
        builder.setContentText(notice.content);
        builder.setTicker(notice.ticker);

        int iconResId = Util.getAppIcon(context);
        builder.setSmallIcon(iconResId);

        RemoteViews rView = null;
        String packageName = context.getPackageName();
        int layoutId = Util.getResIdByName(context, KEY_LAYOUT, KEY_PUSH_NOTIFI_LAYOUT);
        if (layoutId > 0) {
            rView = new RemoteViews(packageName, layoutId);
            int iconId = Util.getResIdByName(context, KEY_ID, KEY_NOTIFI_ICON_ID);
            int titleId = Util.getResIdByName(context, KEY_ID, KEY_NOTIFI_TITLE_ID);
            int contentId = Util.getResIdByName(context, KEY_ID, KEY_NOTIFI_CONTENT_ID);
            int imageLayoutId = Util.getResIdByName(context, KEY_ID, KEY_IMAGE_NOTIFI_PART);

            if (iconId > 0 && titleId > 0 && contentId > 0 && imageLayoutId > 0) {
                rView.setViewVisibility(imageLayoutId, View.GONE);
                if (notice.notify_icon != null) {
                    Bitmap icon = getBitmapFromCache(notice.notify_icon);
                    if (icon != null) {
                        rView.setImageViewBitmap(iconId, icon);
                    } else {
                        rView.setImageViewResource(iconId, iconResId);
                    }
                } else {
                    rView.setImageViewResource(iconId, iconResId);
                }

                rView.setTextViewText(titleId, notice.title);
                rView.setTextViewText(contentId, notice.content);
            } else {
                return null;
            }
        } else {
            PushLog.w("DroiPushHandler image notification layout not found");
            return null;
        }

        if (rView != null) {
            builder.setContent(rView);
        }

        if (notice.vibrate) {
            builder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        if (notice.ring) {
            builder.setDefaults(Notification.DEFAULT_SOUND);
        }

        if (notice.clear) {
            builder.setAutoCancel(true);
        } else {
            builder.setOngoing(true);
        }

        PendingIntent pIntent = getNoticePendingIntent(context, notice);
        if (pIntent != null) {
            builder.setContentIntent(pIntent);
        }

        notification = builder.build();

        if (notice.flashlight) {
            notification.flags = Notification.FLAG_SHOW_LIGHTS;
            notification.ledARGB = Color.GREEN;
            notification.ledOnMS = 300;
            notification.ledOffMS = 300;
        }

        if (Build.VERSION.SDK_INT >= 16) {
            if (notice.big_icon != null && notice.big_icon.length() > 0) {
                int largeImageLayoutId = Util.getResIdByName(context, KEY_LAYOUT, KEY_BIG_IMAGE_NOTIFI_LAYOUT);
                if (largeImageLayoutId > 0) {
                    RemoteViews largeView = new RemoteViews(packageName, largeImageLayoutId);
                    int imageId = Util.getResIdByName(context, KEY_ID, KEY_BIG_IMAGEVIEW_ID);
                    Bitmap icon = getBitmapFromCache(notice.big_icon);
                    if (icon != null) {
                        largeView.setImageViewBitmap(imageId, icon);
                        notification.bigContentView = largeView;
                    }
                }
            }
        }

        return notification;
    }

    @TargetApi(16)
    private Notification buildImageNotification(Context context, DroiPushNotification notice) {
        Notification notification = null;
        if (!notice.isNoticeGetDisplayed(context)) {
            return null;
        }

        int layoutId = Util.getResIdByName(context, KEY_LAYOUT, KEY_PUSH_NOTIFI_LAYOUT);
        if (layoutId == 0) {
            return null;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        int iconId = Util.getAppIcon(context);
        builder.setSmallIcon(iconId);
        builder.setContentTitle(notice.title);
        builder.setContentText(notice.content);
        builder.setTicker(notice.ticker);

        if (notice.vibrate) {
            builder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        if (notice.ring) {
            builder.setDefaults(Notification.DEFAULT_SOUND);
        }

        if (notice.clear) {
            builder.setAutoCancel(true);
        } else {
            builder.setOngoing(true);
        }

        String packageName = context.getApplicationContext().getPackageName();
        if (layoutId > 0) {
            RemoteViews rView = new RemoteViews(packageName, layoutId);
            int simpleLayoutId = Util.getResIdByName(context, KEY_ID, KEY_SIMPLE_NOTIFI_PART);
            int imageId = Util.getResIdByName(context, KEY_ID, KEY_NORMAL_IMAGEVIEW_ID);
            if (imageId > 0 && simpleLayoutId > 0) {
                rView.setViewVisibility(simpleLayoutId, View.GONE);
                if (notice.notify_icon != null) {
                    Bitmap icon = getBitmapFromCache(notice.notify_icon);
                    if (icon != null) {
                        rView.setImageViewBitmap(imageId, icon);
                        builder.setContent(rView);
                    }
                }
            }
        }

        PendingIntent pIntent = getNoticePendingIntent(context, notice);
        if (pIntent != null) {
            builder.setContentIntent(pIntent);
        }

        notification = builder.build();

        if (notice.flashlight) {
            notification.flags = Notification.FLAG_SHOW_LIGHTS;
            notification.ledARGB = Color.GREEN;
            notification.ledOnMS = 300;
            notification.ledOffMS = 300;
        }

        if (Build.VERSION.SDK_INT >= 16) {
            if (notice.big_icon != null && notice.big_icon.length() > 0) {
                int largeImageLayoutId = Util.getResIdByName(context, KEY_LAYOUT, KEY_BIG_IMAGE_NOTIFI_LAYOUT);
                if (largeImageLayoutId > 0) {
                    RemoteViews largeView = new RemoteViews(packageName, largeImageLayoutId);
                    int imageId = Util.getResIdByName(context, KEY_ID, KEY_BIG_IMAGEVIEW_ID);
                    Bitmap icon = getBitmapFromCache(notice.big_icon);
                    if (icon != null) {
                        largeView.setImageViewBitmap(imageId, icon);
                        notification.bigContentView = largeView;
                    }
                }
            }
        }
        return notification;
    }

    private PendingIntent getNoticePendingIntent(Context context, DroiPushNotification notice) {
        String pName = context.getPackageName();
        Intent resultIntent = null;
        String imagePath = notice.action_icon;
        if (notice.action_type == PushMsg.MSG_ACTION_OPENACTIVITY) {
            // open activity
            resultIntent = dealWithOpenActivity(pName, notice.action_data, notice.action_extra_data);
        } else if (notice.action_type == PushMsg.MSG_ACTION_OPENBROWSER) {
            // open url
            resultIntent = dealWithOpenBrowser(pName, notice.action_data);
        } else if (notice.action_type == PushMsg.MSG_ACTION_DOWNLOADAPP) {
            // download apk
            resultIntent = dealWithDownloadAction(pName, notice.action_extra_data, notice.action_data, imagePath);
        }

        if (resultIntent == null) {
            return null;
        }

        resultIntent.putExtra(KEY_MSG_ID, notice.msg_id);
        resultIntent.putExtra(KEY_NOTIFI_TAG, notice.getNoticeTag());
        resultIntent.putExtra(KEY_ID, notice.getNoticeId());

        PendingIntent resultPendingIntent = null;
        resultPendingIntent = PendingIntent.getBroadcast(context, notice.getNoticeId(), resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return resultPendingIntent;

    }

    private Intent dealWithOpenActivity(String localPkg, String activity, String data) {
        Intent resultIntent = new Intent();
        resultIntent.setAction(localPkg + Constants.KEY_ACTION_SUFFIX);
        resultIntent.putExtra(KEY_ACTION_TYPE, PushMsg.MSG_ACTION_OPENACTIVITY);
        resultIntent.putExtra(KEY_ACTIVITY_NAME, activity);
        resultIntent.putExtra(KEY_ACTIVITY_DATA, data);
        return resultIntent;
    }

    private Intent dealWithOpenBrowser(String localPkg, String url) {
        Intent resultIntent = new Intent();
        resultIntent.setAction(localPkg + Constants.KEY_ACTION_SUFFIX);
        resultIntent.putExtra(KEY_ACTION_TYPE, PushMsg.MSG_ACTION_OPENBROWSER);
        resultIntent.putExtra(KEY_WEB_URL, url);
        return resultIntent;
    }

    private Intent dealWithDownloadAction(String localPkg, String apkPkgName, String urlString, String imagePath) {
        if (localPkg == null || urlString == null || apkPkgName == null) {
            return null;
        }
        Intent resultIntent = new Intent();
        resultIntent.setAction(localPkg + Constants.KEY_ACTION_SUFFIX);
        resultIntent.putExtra(KEY_ACTION_TYPE, PushMsg.MSG_ACTION_DOWNLOADAPP);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_PACKAGE_NAME, apkPkgName);
        bundle.putString(KEY_DOWNLOAD_URL, urlString);
        if (imagePath != null && !imagePath.trim().equals("")) {
            bundle.putString(KEY_ILLUSTRATION_URL, imagePath);
        }
        resultIntent.putExtra(KEY_DOWNLOAD_INFO, bundle);
        return resultIntent;
    }

    private Bitmap getBitmapFromCache(String path) {
        Bitmap bitmap = null;
        try {
            bitmap = Utility.getBitmap(path, 0, 0);
        } catch (DroiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            PushLog.e("getBitmapFromCache Exception: " + e.toString());
        }
        return bitmap;
    }
}
