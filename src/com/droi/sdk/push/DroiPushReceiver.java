package com.droi.sdk.push;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import java.io.File;
import com.droi.sdk.DroiException;
import com.droi.sdk.push.download.DownloadTaskManager;
import com.droi.sdk.push.utils.DroiPushBridge;
import com.droi.sdk.push.utils.Util;
import com.droi.sdk.utility.Utility;

public class DroiPushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        try {
            if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
                PushUtil.startPushService(context, intent);
            } else if (Intent.ACTION_PACKAGE_ADDED.equals(action) || Intent.ACTION_PACKAGE_REPLACED.equals(action)) {
                String packageName = intent.getData().getSchemeSpecificPart();
                long msgId = DownloadTaskManager.getInstance(context).getMsgIdOfDownloadedApp(packageName);
                if (msgId > 0) {
                    String deviceId = DroiPushBridge.generateUUID(context);
                    String header = Util.buildAnalyticsHeader();
                    String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, msgId, Constants.ANALYTICS_INSTALL_SUCCESS, deviceId);
                    Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
                }
            } else if (Constants.INTENT_PUSHDATA.equals(action)) {
                handleNotifyReceiver(intent, context);
            } else if ((context.getPackageName() + Constants.KEY_ACTION_SUFFIX).equals(action)) {
                int type = intent.getIntExtra(DroiPushHandler.KEY_ACTION_TYPE, PushMsg.MSGTYPE_INVALID);
                String tag = intent.getStringExtra(DroiPushHandler.KEY_NOTIFI_TAG);
                int id = intent.getIntExtra(DroiPushHandler.KEY_NOTIFI_ID, -1);
                long msgId = intent.getLongExtra(DroiPushHandler.KEY_MSG_ID, -1);

                if (type != PushMsg.MSGTYPE_INVALID) {
                    // send analytics info
                    if (msgId > 0) {
                        String deviceId = DroiPushBridge.generateUUID(context);
                        String header = Util.buildAnalyticsHeader();
                        String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, msgId,
                                Constants.ANALYTICS_CLICK_NOTIFICATION, deviceId);
                        Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
                    }

                    if (type == PushMsg.MSG_ACTION_OPENACTIVITY) {
                        DroiPush.getInstance().onNotificationClicked(msgId, tag, id);
                        String activity = intent.getStringExtra(DroiPushHandler.KEY_ACTIVITY_NAME);
                        String data = intent.getStringExtra(DroiPushHandler.KEY_ACTIVITY_DATA);
                        openActivity(context, activity, data);
                    } else if (type == PushMsg.MSG_ACTION_OPENBROWSER) {
                        DroiPush.getInstance().onNotificationClicked(msgId, tag, id);
                        String url = intent.getStringExtra(DroiPushHandler.KEY_WEB_URL);
                        openURL(context, url);
                    } else if (type == PushMsg.MSG_ACTION_DOWNLOADAPP) {
                        DroiPush.getInstance().onNotificationClicked(msgId, tag, id);
                        Bundle bundle = intent.getBundleExtra(DroiPushHandler.KEY_DOWNLOAD_INFO);
                        String urlString = bundle.getString(DroiPushHandler.KEY_DOWNLOAD_URL);
                        String pkgName = bundle.getString(DroiPushHandler.KEY_PACKAGE_NAME);
                        String imagePath = "";
                        try {
                            imagePath = bundle.getString(DroiPushHandler.KEY_ILLUSTRATION_URL);
                        } catch (Exception e) {
                            imagePath = null;
                        }

                        if (!Util.isStringValid(urlString) || !Util.isStringValid(pkgName)) {
                            return;
                        }

                        if (Util.isStringValid(imagePath)) {
                            try {
                                if (Utility.isBitmapCached(imagePath)) {
                                    showAppView(context, msgId, id, pkgName, urlString, imagePath);
                                    return;
                                }
                            } catch (DroiException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        DownloadTaskManager.getInstance(context).startDownload(msgId, id, urlString, pkgName);
                    }
                    DroiPushBridge.onNotificationClick(type);
                }
            } else if (Intent.ACTION_USER_PRESENT.equals(action)
                    || ConnectivityManager.CONNECTIVITY_ACTION.equals(action)
                    || TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(action)) {
                Intent i = new Intent(Constants.INTENT_PUSHSERVICE);
                if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                    i.putExtra(Constants.KEY_CMD, Constants.KEY_NETWORK_CHANGE);
                }
                PushUtil.startPushService(context, i);
            }
        } catch (Exception e) {
        }
    }

    private void handleNotifyReceiver(Intent intent, final Context context) throws DroiException {
        if (intent == null) {
            return;
        }

        boolean isPushEnable = PushSetting.getInstance(context).getIsPushEnabled();
        if (!isPushEnable) {
            return;
        }

        String action = intent.getAction();
        if (Constants.INTENT_PUSHDATA.equals(action)) {
            String destAppKey = intent.getStringExtra(DroiPushService.KEY_APP_KEY);
            String curAppKey = Util.getAppKey(context);
            if (curAppKey != null && curAppKey.equals(destAppKey)) {
                String msgJson = intent.getStringExtra(DroiPushService.KEY_MESSAGE);
                String msgTag = intent.getStringExtra(DroiPushService.KEY_MESSAGE_TAG);
                if (Util.isStringValid(msgJson)) {
                    DroiPushNotification notice = new DroiPushNotification(msgTag, msgJson);
                    DroiPush.getInstance().notifyExt(notice);
                }
            }
        }
    }

    public void openApp(Context context, String packageName) {
        if (packageName == null) {
            return;
        }
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return;
        }
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
    }

    public void openActivity(Context context, String activityName, String data) {
        if (!Util.isStringValid(activityName)) {
            String packageName = context.getApplicationContext().getPackageName();
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            if (Util.isStringValid(data)) {
                intent.putExtra(DroiPushHandler.KEY_ACTIVITY_PASS_DATA, data);
            }
            intent.setAction(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } else {
            Intent i = new Intent();
            if (Util.isStringValid(data)) {
                i.putExtra(DroiPushHandler.KEY_ACTIVITY_PASS_DATA, data);
            }
            String packageName = context.getApplicationInfo().packageName;
            i.setComponent(new ComponentName(packageName, packageName + "." + activityName));
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
        }
    }

    public void openURL(Context context, String url) {
        if (url == null) {
            return;
        }
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        String[] browser = { "com.tencent.mtt", "com.UCMobile", "com.uc.browser", "com.qihoo.browser",
                "com.baidu.browser.apps", "com.oupeng.browser", "com.oupeng.mini.android", "com.android.browser" };

        for (String br : browser) {
            if (Util.isAppInstalled(context, br)) {
                PackageManager pm = context.getApplicationContext().getPackageManager();
                if (pm.getLaunchIntentForPackage(br) != null) {
                    i.setPackage(br);
                    break;
                }
            }
        }
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public void showAppView(Context context, long msgId, int requestId, String compoundName, String downloadUrl, String imageUrl) {
        Intent intent = new Intent(context, DroiPushActivity.class);
        intent.putExtra(DroiPushActivity.KEY_MESSAGE_ID, msgId);
        intent.putExtra(DroiPushActivity.KEY_REQUEST_ID, requestId);
        intent.putExtra(DroiPushActivity.KEY_IMAGE_URL, imageUrl);
        intent.putExtra(DroiPushActivity.KEY_DOWNLOAD_URL, downloadUrl);
        intent.putExtra(DroiPushActivity.KEY_PACKAGE_NAME, compoundName);
        intent.setAction(Constants.INTENT_SHOW_APP_DETAIL);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
