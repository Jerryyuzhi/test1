﻿package com.droi.sdk.push.download;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.droi.sdk.push.Constants;
import com.droi.sdk.push.DroiPush;
import com.droi.sdk.push.utils.CPResourceUtil;
import com.droi.sdk.push.utils.DroiPushBridge;
import com.droi.sdk.push.utils.FilePathUtil;
import com.droi.sdk.push.utils.PushLog;
import com.droi.sdk.push.utils.Util;
import com.droi.sdk.utility.DroiDownloadFile;
import com.droi.sdk.utility.DroiDownloadFile.DroiDownloadFileEventListener;

public class DownloadTaskManager {

    // keyword for download file path
    public static final String KEY_FILE_PATH = "filePath";
    public static final int MAX_DOWNLOADED_INFO_NUM = 100;

    // keywords for download notification layout
    private static final String DOWNLOAD_NOTIFI_LAYOUT = "dp_download_notification_layout";
    private static final String NOTIFI_ICON_ID = "dp_download_icon";
    private static final String NOTIFI_TITLE_ID = "dp_download_title";
    private static final String NOTIFI_CONTENT_ID = "dp_download_text";
    private static final String NOTIFI_PROGRESS_GROUP_ID = "dp_progress_group";
    private static final String NOTIFI_PROGRESS_BAR_ID = "dp_download_progressbar";
    private static final String NOTIFI_PROGRESS_NUM_ID = "dp_download_progress";

    // keywords for the hint text of download result
    public static final String DOWNLOAD_SUCCESS_TEXT_ID = "dp_download_success_text";
    public static final String DOWNLOAD_FAILED_TEXT_ID = "dp_download_fail_text";
    public static final String NO_SDCARD_HINT_ID = "dp_nosdcard_text";

    // default download hint text
    public static final String DEFAULT_DOWNLOAD_SUCCESS_HINT = "Download successfully, click to install!";
    public static final String DEFAULT_DOWNLOAD_FAIL_HINT = "Download failed!";
    public static final String DEFAULT_NO_SDACRD_HINT = "SD card bit found, insert the SD card and try again";

    private static DownloadTaskManager instance = null;
    private Context mContext;
    private NotificationManager mNotifyManager;
    private Map<String, TaskInfo> mDownloadingInfo = new HashMap<String, TaskInfo>();
    private String mDownloadRootPath = FilePathUtil.getPushSdcardPath();

    // save downloaded app package name and message id
    private Map<String, Long> mDownloadedApp = new HashMap<String, Long>();
    private String[] mDownloadedPackageNamePool = new String[MAX_DOWNLOADED_INFO_NUM];
    private int mPosition = -1;

    private DownloadTaskManager(Context context) {
        PushLog.d("initTask");
        mContext = context.getApplicationContext();
        mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static DownloadTaskManager getInstance(Context context) {
        synchronized (DownloadTaskManager.class) {
            if (instance == null) {
                instance = new DownloadTaskManager(context);
            }
        }
        return instance;
    }

    public int startDownload(long msgId, int requestId, String url, String compoundName) {
        int taskId = -1;

        if (!Util.isStringValid(url)) {
            PushLog.d("Download url is invalid, abort!");
            return -1;
        }

        if (!Util.isStringValid(compoundName)) {
            PushLog.d("Compound name is invalid, abort!");
            return -1;
        }

        // app is downloading now, ignore
        if (mDownloadingInfo.containsKey(url)) {
            return 0;
        }

        String pkgName = null;
        String appName = null;
        String[] names = compoundName.split("\\|");
        pkgName = names[0];
        if (names.length == 2) {
            appName = names[1];
        }

        if (!Util.isStringValid(pkgName)) {
            PushLog.d("Invalid package name in message: " + msgId);
            return -1;
        }

        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            int noSDCardHintTextId = CPResourceUtil.getStringId(mContext, NO_SDCARD_HINT_ID);
            String text = DEFAULT_NO_SDACRD_HINT;
            if (noSDCardHintTextId > 0) {
                text = mContext.getString(noSDCardHintTextId);
            }
            Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
            return -1;
        }

        String rootPath = FilePathUtil.getPushSdcardPath();
        File rootDir = new File(rootPath);
        if (!rootDir.exists()) {
            rootDir.mkdirs();
        }

        // app file exist, install immediately
        String filePath = null;
        try {
            filePath = rootPath + "/" + Util.md5(url);
            boolean isApkExist = installAppIfExist(filePath, msgId);
            if (isApkExist) {
                return Integer.MAX_VALUE;
            }
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return -1;
        }

        TaskInfo info = new TaskInfo();
        info.msgId = msgId;
        info.id = requestId;
        info.appName = appName;
        info.packageName = pkgName;
        mDownloadingInfo.put(url, info);

        try {
            taskId = DroiDownloadFile.instance().downloadFile(url, rootPath + "/" + Util.md5(url), dlCallback);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            mDownloadingInfo.remove(url);
            return -1;
        }
        return taskId;
    }

    private final DroiDownloadFileEventListener dlCallback = new DroiDownloadFileEventListener() {
        @Override
        public void onFinished(final String url, final String destinationFileName) {
            // TODO Auto-generated method stub
            PushLog.d("download success: " + url);
            TaskInfo info = mDownloadingInfo.get(url);
            if (info != null) {
                insertOrUpdateDownloadedInfo(info.packageName, info.msgId);
                if (info.msgId > 0) {
                    String deviceId = DroiPushBridge.generateUUID(mContext);
                    String header = Util.buildAnalyticsHeader();
                    String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, info.msgId, Constants.ANALYTICS_DOWNLOAD_SUCCESS, deviceId);
                    Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
                }

                boolean isDownloadSuccess = false;
                try {
                    String filePath = mDownloadRootPath + "/" + Util.md5(url);
                    isDownloadSuccess = installAppIfExist(filePath, info.msgId);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDownloadSuccess) {
                    showNotification(url, 100, null);
                } else {
                    PendingIntent pIntent = PendingIntent.getBroadcast(mContext, info.id, new Intent(),
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    showNotification(url, -1, pIntent);
                }
            }
            mDownloadingInfo.remove(url);
        }

        @Override
        public void onStart(final String url, long startPos) {
            // TODO Auto-generated method stub
            PushLog.d("download onStart:" + url);
            showNotification(url, 0, null);
        }

        @Override
        public void onFailed(final String url) {
            PushLog.d("download onFailed:" + url);
            TaskInfo info = mDownloadingInfo.get(url);
            if (info != null) {
                PendingIntent pIntent = PendingIntent.getBroadcast(mContext, info.id, new Intent(),
                        PendingIntent.FLAG_UPDATE_CURRENT);
                showNotification(url, -1, pIntent);
            }
            mDownloadingInfo.remove(url);
        }

        @Override
        public void onProgress(final String url, float progress) {
            int percent = 0;
            if (progress > 0 && progress < 1) {
                percent = (int) (progress * 100L);
            } else {
                percent = 0;
            }
            showNotification(url, percent, null);
        }
    };

    private void showNotification(String url, int progress, PendingIntent pIntent) {
        TaskInfo info = mDownloadingInfo.get(url);
        if (info != null) {
            Notification notify = info.notice;
            if (notify == null) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
                notify = builder.setTicker(null).setSmallIcon(CPResourceUtil.getDrawableId(mContext, "ic_launcher"))
                        .setContentText(null).setContentTitle(null).build();
                notify.flags = Notification.FLAG_NO_CLEAR;
                info.notice = notify;
            }

            if (progress < 0) {
                int infoId = CPResourceUtil.getId(mContext, NOTIFI_CONTENT_ID);
                int progressGroupId = CPResourceUtil.getId(mContext, NOTIFI_PROGRESS_GROUP_ID);
                int failHintTextId = CPResourceUtil.getStringId(mContext, DOWNLOAD_FAILED_TEXT_ID);
                RemoteViews remoteView = notify.contentView;

                if(progressGroupId > 0 && infoId > 0){
                    remoteView.setViewVisibility(progressGroupId, View.GONE);
                    remoteView.setViewVisibility(infoId, View.VISIBLE);
                }

                String text = DEFAULT_DOWNLOAD_FAIL_HINT;
                if (failHintTextId > 0) {
                    text = mContext.getString(failHintTextId);
                }
                remoteView.setTextViewText(infoId, text);
                notify.flags = Notification.FLAG_AUTO_CANCEL;
                notify.contentIntent = pIntent;

                Toast.makeText(mContext, DEFAULT_DOWNLOAD_FAIL_HINT, Toast.LENGTH_SHORT).show();
            } else if (progress >= 100) {
                mNotifyManager.cancel(url, info.id);
                Toast.makeText(mContext, DEFAULT_DOWNLOAD_SUCCESS_HINT, Toast.LENGTH_SHORT).show();
                return;
            } else {
                RemoteViews remoteView = new RemoteViews(mContext.getPackageName(),
                        CPResourceUtil.getLayoutId(mContext, DOWNLOAD_NOTIFI_LAYOUT));
                Bitmap bp = getIcon();
                int iconId = CPResourceUtil.getId(mContext, NOTIFI_ICON_ID);
                int titleId = CPResourceUtil.getId(mContext, NOTIFI_TITLE_ID);
                remoteView.setImageViewBitmap(iconId, bp);
                remoteView.setTextViewText(titleId, info.getAppName());

                int infoId = CPResourceUtil.getId(mContext, NOTIFI_CONTENT_ID);
                int progressGroupId = CPResourceUtil.getId(mContext, NOTIFI_PROGRESS_GROUP_ID);
                int progressBarId = CPResourceUtil.getId(mContext, NOTIFI_PROGRESS_BAR_ID);
                int progressNumId = CPResourceUtil.getId(mContext, NOTIFI_PROGRESS_NUM_ID);
                remoteView.setViewVisibility(progressGroupId, View.VISIBLE);
                remoteView.setViewVisibility(infoId, View.GONE);
                remoteView.setTextViewText(progressNumId, progress + "%");
                remoteView.setProgressBar(progressBarId, 100, progress, false);
                notify.contentView = remoteView;
            }

            mNotifyManager.notify(url, info.id, notify);
        }
    }

    private Bitmap getIcon() {
        PackageManager pm = mContext.getPackageManager();
        try {
            ApplicationInfo info = pm.getApplicationInfo(mContext.getPackageName(), 0);
            Drawable drawable = info.loadIcon(pm);
            return ((BitmapDrawable) drawable).getBitmap();
        } catch (NameNotFoundException e) {
            PushLog.e(e);
            return null;
        }
    }

    private void insertOrUpdateDownloadedInfo(String packageName, long msgId) {
        if (!Util.isStringValid(packageName) || msgId < 0) {
            return;
        }

        if (!mDownloadedApp.containsKey(packageName)) {
            int next = (mPosition + 1) % MAX_DOWNLOADED_INFO_NUM;
            if (next < mPosition) {
                mDownloadedApp.remove(mDownloadedPackageNamePool[next]);
            }
            mDownloadedPackageNamePool[next] = packageName;
            mDownloadedApp.put(packageName, msgId);
            mPosition = next;
        } else {
            mDownloadedApp.put(packageName, msgId);
        }
    }

    public long getMsgIdOfDownloadedApp(String packageName) {
        Long id = mDownloadedApp.get(packageName);
        return id == null ? -1 : id.longValue();
    }

    private boolean installAppIfExist(String filePath, long messageId) {
        boolean isApkValid = false;
        if(!Util.isStringValid(filePath)){
            PushLog.d("Invalid app file path: " + filePath);
            return isApkValid;
        }

        File appFile = new File(filePath);
        if (appFile.exists()) {
            // send analytics info
            isApkValid = Util.installApkFile(mContext, appFile);
            if (isApkValid && messageId > 0) {
                String deviceId = DroiPushBridge.generateUUID(mContext);
                String header = Util.buildAnalyticsHeader();
                String content = Util.buildAnalyticsContent(Constants.ANALYTICS_TYPE_01, messageId,
                        Constants.ANALYTICS_SHOW_INSTALL_WINDOW, deviceId);
                Util.sendAnalyticsInfo(DroiPush.mAnalyticsCollector, header, content);
            }
        }
        return isApkValid;
    }

    class TaskInfo {
        long msgId = -1;
        int id;
        String appName;
        String packageName;
        Notification notice;

        String getAppName() {
            if (Util.isStringValid(appName)) {
                return appName;
            }
            return packageName;
        }
    }
}
