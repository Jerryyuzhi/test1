package com.droi.sdk.push.utils;

import android.content.Context;
import com.droi.sdk.core.PushCoreHelper;

public class DroiPushBridge {

    public static String generateUUID(Context ctx) {
        String uuid = null;
        uuid = PushCoreHelper.getDeviceId(ctx);
        if (uuid != null) {
            return uuid.replace("-", "");
        }
        return uuid;
    }

    public static byte[] encryptData(String key, byte[] data) {
        if (data == null) {
            return null;
        }
        try {
            return DESUtil.encrypt(data, key.getBytes());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
            return null;
        }
    }

    public static byte[] decryptData(String key, byte[] data) {
        try {
            return DESUtil.decrypt(data, key.getBytes());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            PushLog.v("Decrypt data error, Check App Key and Secret");
            PushLog.e(e);
            return null;
        }
    }

    public static void onNotificationClick(int type) {
        PushLog.v("Clicked on type:" + type);
        return;
    }
}
