package com.droi.sdk.push.utils;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageManager;

import com.droi.sdk.internal.DroiDataCollector;

public class DeviceInfo {
    private static final String KEY_DEVICE_ID = "deviceId";
    private static final String KEY_IMSI = "imsi";
    private static final String KEY_IMEI = "imei";
    private static final String KEY_DEVICE_MANUFACTURER = "handsetMan";
    private static final String KEY_DEVICE_TYPE = "handsetType";
    private static final String KEY_OS_VERSION = "osVer";
    private static final String KEY_SCREEN_WIDTH = "screenWidth";
    private static final String KEY_SCREEN_HEIGHT = "screenHeight";
    private static final String KEY_CPU = "cpu";
    private static final String KEY_RAM = "ram";
    private static final String KEY_ROM = "rom";
    private static final String KEY_MAC = "mac";
    private static final String KEY_NETWORK_TYPE = "networkType";

    private static final String[] REQUIRED_PERMISSONS = { "android.permission.ACCESS_NETWORK_STATE",
            "android.permission.ACCESS_WIFI_STATE", "android.permission.READ_PHONE_STATE",
            "android.permission.ACCESS_COARSE_LOCATION", "android.permission.WRITE_EXTERNAL_STORAGE" };

    public DeviceInfo() {
        // TODO Auto-generated constructor stub
    }

    public static boolean hasPermission(Context context, String permission) {
        return (context.checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
    }

    public static void requirePermission(Context context, String permission) {
        if (!hasPermission(context, permission)) {
            throw new IllegalStateException("\nTo use this library, add this to your AndroidManifest.xml:\n"
                    + "<uses-permission android:name=\"" + permission + "\" />");
        }
    }

    public static void requirePermissionByArray(Context context, String[] permissions) {
        for (String permission : permissions) {
            requirePermission(context, permission);
        }
    }

    public static String get(Context context) {
        requirePermissionByArray(context, REQUIRED_PERMISSONS);
        JSONObject object = new JSONObject();
        try {
            object.put(KEY_DEVICE_ID, DroiPushBridge.generateUUID(context));
            object.put(KEY_IMSI, DroiDataCollector.getImsi(context));
            object.put(KEY_IMEI, DroiDataCollector.getImei(context));
            object.put(KEY_DEVICE_MANUFACTURER, DroiDataCollector.getBuildBrand());
            object.put(KEY_DEVICE_TYPE, DroiDataCollector.getBuildModel());
            object.put(KEY_OS_VERSION, DroiDataCollector.getCustomVersion());
            object.put(KEY_SCREEN_WIDTH, DroiDataCollector.getLcdWidth(context));
            object.put(KEY_SCREEN_HEIGHT, DroiDataCollector.getLcdHeight(context));
            object.put(KEY_CPU, DroiDataCollector.getBuildHardware());
            object.put(KEY_RAM, DroiDataCollector.getTatalRAMSize());
            object.put(KEY_ROM, DroiDataCollector.getTotalRomSize());
            object.put(KEY_MAC, DroiDataCollector.getWifiMAC(context));
            object.put(KEY_NETWORK_TYPE, DroiDataCollector.getCurNetworkType(context));
            return object.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
            return null;
        }
    }
}
