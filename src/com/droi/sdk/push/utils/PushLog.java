/** 
 * Utility class for LogCat.
 *
 */
package com.droi.sdk.push.utils;

import com.droi.sdk.internal.DroiLog;

public class PushLog {
    public static final String TAG_PUSH = "DROI_PUSH";

    public static void d(String msg) {
        DroiLog.d(TAG_PUSH, msg);
    }

    public static void v(String msg) {
        DroiLog.v(TAG_PUSH, msg);
    }

    public static void e(String msg) {
        DroiLog.e(TAG_PUSH, msg);
    }

    public static void i(String msg) {
        DroiLog.i(TAG_PUSH, msg);
    }

    public static void w(String msg) {
        DroiLog.w(TAG_PUSH, msg);
    }

    public static void e(Exception ex) {
        DroiLog.e(TAG_PUSH, ex);
    }

}
