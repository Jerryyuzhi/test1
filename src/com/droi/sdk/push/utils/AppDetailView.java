package com.droi.sdk.push.utils;

import com.droi.sdk.push.DroiPushActivity.DownloadListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class AppDetailView extends RelativeLayout {
    private final static String DOWNLOAD_HINT_TEXT_ID = "dp_download_install_text";
    private final static String DEFAULT_DOWNLOAD_HINT_TEXT = "Download and install";

    private Context mContext;
    private ImageView mImage;
    private Button mStart;
    private Bitmap mBitmap;
    private int mScreenWidth;
    private int mScreenHeight;
    private int mBitmapWidth;
    private int mBitmapHeight;

    private DownloadListener mDownloadListener;

    @TargetApi(16)
    public AppDetailView(Context context, Bitmap bitmap) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = context;
        mBitmap = bitmap;

        if (mBitmap != null) {
            mBitmapWidth = mBitmap.getWidth();
            mBitmapHeight = mBitmap.getHeight();
        }

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;

        mImage = new ImageView(mContext.getApplicationContext());
        mImage.setScaleType(ScaleType.CENTER_CROP);
        mImage.setId(1);
        LayoutParams lp1 = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp1.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp1.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        mImage.setLayoutParams(lp1);
        mImage.setImageBitmap(mBitmap);
        addView(mImage);

        mStart = new Button(mContext.getApplicationContext());
        mStart.setTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= 16) {
            mStart.setBackground(getBackgroundDrawable());
        } else {
            mStart.setBackgroundDrawable(getBackgroundDrawable());
        }
        mStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mDownloadListener != null) {
                    mDownloadListener.onDownloadStart();
                }
            }
        });

        LayoutParams lp2 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp2.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp2.addRule(RelativeLayout.BELOW, 1);

        int noButtonTextId = CPResourceUtil.getStringId(mContext, DOWNLOAD_HINT_TEXT_ID);
        String text = DEFAULT_DOWNLOAD_HINT_TEXT;
        if (noButtonTextId > 0) {
            text = mContext.getString(noButtonTextId);
        }
        mStart.setLayoutParams(lp2);
        mStart.setText(text);

        addView(mStart);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int finalWidth = mBitmapWidth;
        int finalHeight = mBitmapHeight;

        float scaleBitmap = (float) mBitmapWidth / mBitmapHeight;
        float scaleScreen = (float) mScreenWidth / mScreenHeight;

        if (scaleBitmap >= scaleScreen) {
            finalWidth = (int) (mScreenWidth * 0.8f);
            finalHeight = mBitmapHeight * finalWidth / mBitmapWidth;
        } else {
            finalHeight = (int) (mScreenHeight * 0.8f);
            finalWidth = mScreenWidth * finalHeight / mScreenHeight;
        }

        int imageWidthSpec = MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY);
        int imageHeightSpec = MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY);
        mImage.measure(imageWidthSpec, imageHeightSpec);
        LayoutParams lp1 = (LayoutParams) mImage.getLayoutParams();
        if (lp1 != null) {
            lp1.width = finalWidth;
            lp1.height = finalHeight;
        }

        int startWidthSpec = MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY);
        int startHeightSpec = MeasureSpec.makeMeasureSpec((int) (mScreenHeight - finalHeight), MeasureSpec.AT_MOST);
        mStart.measure(startWidthSpec, startHeightSpec);
        int btnHeight = mStart.getMeasuredHeight();
        LayoutParams lp2 = (LayoutParams) mStart.getLayoutParams();
        if (lp2 != null) {
            lp2.width = finalWidth;
            lp2.height = btnHeight;
        }
        setMeasuredDimension(finalWidth, finalHeight + btnHeight);
    }

    public void setDownloadListener(DownloadListener listener) {
        mDownloadListener = listener;
    }

    private StateListDrawable getBackgroundDrawable() {
        StateListDrawable bg = new StateListDrawable();
        ColorDrawable press = new ColorDrawable(0xcc69b8f7);
        ColorDrawable normal = new ColorDrawable(0xff69b8f7);
        bg.addState(View.PRESSED_ENABLED_STATE_SET, press);
        bg.addState(View.ENABLED_FOCUSED_STATE_SET, press);
        bg.addState(View.FOCUSED_STATE_SET, press);
        bg.addState(View.ENABLED_STATE_SET, normal);
        bg.addState(View.EMPTY_STATE_SET, normal);
        return bg;
    }

    @Override
    protected void onDetachedFromWindow() {
        // TODO Auto-generated method stub
        super.onDetachedFromWindow();
        if (mBitmap != null) {
            mBitmap.recycle();
            mBitmap = null;
        }
    }
}