package com.droi.sdk.push.utils;

import android.content.Context;
import android.os.Environment;

import com.droi.sdk.push.Constants;

public class FilePathUtil {
    public static String getPushSdcardPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.SDCARD_DRODPUSH_DIR;
    }

    public static String getSDCardCachePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.SDCARD_DRODPUSH_DIR
                + Constants.SDCARD_RESCACHE_DIR;
    }

    public static String getPushLogLevelFilePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.LEVEL_FILE_NAME;
    }

    public static String getExceptionLogFilePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.SDCARD_LOG_FILE_NAME;
    }

    public static String getPushAppPath(Context context) {
        if (context == null) {
            PushLog.d("getPushAppPath ---context is null!");
            return null;
        }
        return context.getFilesDir().getAbsolutePath() + "/";
    }

    public static String getCpTestFilePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/droi/ThirdParty_Test";
    }
}
