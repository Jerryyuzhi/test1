package com.droi.sdk.push.utils;

import java.io.UnsupportedEncodingException;

import android.util.Base64;

public class Base64Util {
    public static String DecodeFromBase64Data(String data) {
        String result = null;
        if (data == null || data.length() == 0) {
            return null;
        }

        byte[] bytes = Base64.decode(data, Base64.DEFAULT);
        try {
            result = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }
        return result;
    }

    public static String EncodeIntoBase64(String data) {
        String result = null;

        if (data == null || data.length() == 0) {
            return null;
        }

        byte[] bytes;
        try {
            bytes = data.getBytes("UTF-8");
            result = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
        }
        return result;
    }
}