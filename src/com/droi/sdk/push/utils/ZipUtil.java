package com.droi.sdk.push.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ZipUtil {

    /**
     * Method to compress data
     * 
     * @param byteArray
     *            the source data
     * @return the compressed data
     * @throws IOException
     */
    public static byte[] compress(byte[] byteArray) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        gzip.write(byteArray);
        gzip.close();
        byte[] compressByteArray = out.toByteArray();
        return compressByteArray;
    }

    /**
     * Method to decompress data
     * 
     * @param byteArry
     *            the source data to be decompressed
     * @return the decompressed data
     * @throws IOException
     */
    public static byte[] decompress(byte[] byteArry) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(byteArry);
        GZIPInputStream gunzip = new GZIPInputStream(in);
        byte[] buffer = new byte[256];
        int n;
        while ((n = gunzip.read(buffer)) >= 0) {
            out.write(buffer, 0, n);
        }
        return out.toByteArray();
    }
}
