package com.droi.sdk.push.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Pair;

import com.droi.sdk.DroiException;
import com.droi.sdk.analytics.priv.AnalyticsModule;
import com.droi.sdk.internal.DroiDataCollector;
import com.droi.sdk.internal.DroiLog;
import com.droi.sdk.push.Constants;
import com.droi.sdk.push.data.ServiceIpsBean;

public class Util {
    private static final String KEY_APP_KEY = "appKey";
    private static final String KEY_ANALYTICS_TYPE = "mt";
    private static final String KEY_ANALYTICS_CONTENT = "mc";
    private static final String KEY_ANALYTICS_MSGID = "mi";
    private static final String KEY_ANALYTICS_ACTION = "ma";
    private static final String KEY_ANALYTICS_TIME = "ti";
    private static final String KEY_ANALYTICS_DEVID = "di";

    public static char convertDigit(int value) {
        value &= 0x0f;
        if (value >= 10) {
            return ((char) (value - 10 + 'a'));
        } else {
            return ((char) (value + '0'));
        }
    }

    public static String convert(final byte bytes[]) {
        StringBuffer sb = new StringBuffer(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++) {
            sb.append(convertDigit((int) (bytes[i] >> 4)));
            sb.append(convertDigit((int) (bytes[i] & 0x0f)));
        }
        return (sb.toString());
    }

    public static String convert(final byte bytes[], int pos, int len) {
        StringBuffer sb = new StringBuffer(len * 2);
        for (int i = pos; i < pos + len; i++) {
            sb.append(convertDigit((int) (bytes[i] >> 4)));
            sb.append(convertDigit((int) (bytes[i] & 0x0f)));
        }
        return (sb.toString());
    }

    public static byte[] md5Byte(String encryptStr) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(encryptStr.getBytes("UTF-8"));
        return md.digest();
    }

    public static String md5(String encryptStr) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(encryptStr.getBytes("UTF-8"));
        byte[] digest = md.digest();
        StringBuffer md5 = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            md5.append(Character.forDigit((digest[i] & 0xF0) >> 4, 16));
            md5.append(Character.forDigit((digest[i] & 0xF), 16));
        }
        encryptStr = md5.toString();
        return encryptStr;
    }

    public static boolean hasNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static String getAppKey(Context context) {
        return getMetaData(context, Constants.KEY_CONFIG_APPKEY);
    }

    public static String getSecret(Context context) {
        String secret = getMetaData(context, Constants.KEY_CONFIG_SECRET);
        if (secret == null) {
            secret = Constants.DEFAULT_SECRET;
        }
        return secret;
    }

    public static String getChannel(Context context) {
        String ch = getMetaData(context, Constants.KEY_CONFIG_CHANNEL);
        if (ch == null) {
            ch = Constants.DEFAULT_CHANNEL;
        }
        return ch;
    }

    public static String getTransferMode(Context context) {
//        String mode = getMetaData(context, Constants.KEY_TRANSFER_MODE);
//        if (mode == null) {
//            mode = Constants.TRANSFER_MODE_UDP;
//        }
//        return mode;
        return Constants.TRANSFER_MODE_UDP;
    }

    private static String getMetaData(Context context, String key) {
        String data = null;
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA);
            if (appInfo != null && appInfo.metaData != null) {
                data = appInfo.metaData.getString(key);
            }
        } catch (NameNotFoundException e) {
            PushLog.v("Error to Get Meta Data: " + key);
        }
        return data;
    }

//    public static String postData(Context context, String urlString, String contents) throws IOException {
//        return postData(context, urlString, contents, "x_s0_s22");
//    }

    public static String postData(Context context, String urlString, String contents, String key) throws IOException {
        return postData(context, urlString, contents, key, null);
    }

    public static String postData(Context context, String urlString, String data, String key, String appKey)
            throws IOException {
        String line = "";
        DataOutputStream out = null;
        BufferedInputStream bis = null;
        ByteArrayBuffer baf = null;
        boolean isPress = false;
        HttpURLConnection connection = null;
        try {
            byte[] encrypted = DESUtil.encrypt(data.getBytes("UTF-8"), key.getBytes());
            if (urlString.startsWith("http://")) {
                URL postUrl = new URL(urlString);
                connection = (HttpURLConnection) postUrl.openConnection();
            } else {
                String[] addresses = loadHttpServerAddress();
                for (int i = 0; i < addresses.length; i++) {
                    String address = "http://" + addresses[i] + urlString;
                    URL postUrl = new URL(address);
                    try {
                        connection = (HttpURLConnection) postUrl.openConnection();
                        break;
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }

            if (connection == null) {
                throw new IOException("Create connection fialed!");
            }

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("contentType", "UTF-8");
            if (appKey != null) {
                connection.setRequestProperty(KEY_APP_KEY, appKey);
            }
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + encrypted.length);

            out = new DataOutputStream(connection.getOutputStream());
            out.write(encrypted);
            out.flush();

            bis = new BufferedInputStream(connection.getInputStream());
            baf = new ByteArrayBuffer(1024);

            isPress = Boolean.valueOf(connection.getHeaderField("isPress"));
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            if (baf.length() > 0) {
                byte unCompressByte[];
                byte[] decrypted;
                if (isPress) {
                    decrypted = DESUtil.decrypt(baf.toByteArray(), key.getBytes());
                    unCompressByte = ZipUtil.decompress(decrypted);
                    line = new String(unCompressByte, "UTF-8");
                } else {
                    decrypted = DESUtil.decrypt(baf.toByteArray(), key.getBytes());
                    line = new String(decrypted);
                }
            }
        } catch (Exception e) {
            PushLog.e(e);
        } finally {
            try {
                if (connection != null) {
                    connection.disconnect();
                }
                if (bis != null) {
                    bis.close();
                }
                if (baf != null) {
                    baf.clear();
                }
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
                PushLog.e(e);
            }
        }
        return line.trim();
    }

    public static HttpURLConnection postUpdateData(Context context, String urlString, String data, String key,
            String appKey) throws IOException {
        DataOutputStream out = null;
        HttpURLConnection connection = null;
        try {
            byte[] encrypted = DESUtil.encrypt(data.getBytes("UTF-8"), key.getBytes());
            URL postUrl = new URL(urlString);
            connection = (HttpURLConnection) postUrl.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("contentType", "UTF-8");
            if (appKey != null) {
                connection.setRequestProperty(KEY_APP_KEY, appKey);
            }
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + encrypted.length);

            out = new DataOutputStream(connection.getOutputStream());
            out.write(encrypted);
            out.flush();

            connection.connect();
        } catch (Exception e) {
            PushLog.e(e);
        } finally {
            try {
                if (connection != null) {
                    connection.disconnect();
                }
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
                PushLog.w(e.toString());
            }
        }
        return connection;
    }

    public static byte[] bytesFromStream(HttpURLConnection connection, String key) {
        BufferedInputStream bis = null;
        ByteArrayBuffer baf = null;
        boolean isPress = false;
        try {
            bis = new BufferedInputStream(connection.getInputStream());
            baf = new ByteArrayBuffer(1024);
            isPress = Boolean.valueOf(connection.getHeaderField("isPress"));
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            if (baf.length() > 0) {
                byte unCompressByte[];
                byte[] decrypted;
                if (isPress) {
                    decrypted = DESUtil.decrypt(baf.toByteArray(), key.getBytes());
                    unCompressByte = ZipUtil.decompress(decrypted);
                    return unCompressByte;
                } else {
                    decrypted = DESUtil.decrypt(baf.toByteArray(), key.getBytes());
                    return decrypted;
                }
            }
        } catch (Exception e) {
            PushLog.e(e);
        } finally {
            try {
                if (bis != null)
                    bis.close();
                if (baf != null)
                    baf.clear();
            } catch (Exception e) {
                PushLog.e(e);
            }
        }
        return new byte[0];
    }

    public static boolean saveStream(InputStream input, String path) {
        FileOutputStream output;
        try {
            new File(path.substring(0, path.lastIndexOf("/"))).mkdirs();
            output = new FileOutputStream(path);
            int count;
            byte buff[] = new byte[1024];
            while ((count = input.read(buff, 0, 1024)) != -1) {
                output.write(buff, 0, count);
            }
            input.close();
            output.close();
            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            PushLog.e(e);
            return false;
        }
    }

    /**
     * Save serializable object into file in SD card
     * 
     * @param obj
     *            the saved object
     * @param fileName
     *            the file name
     * @return save successful or failed
     */
    public static boolean saveSerializableToSD(Serializable obj, String fileName) {
        if (!isExistSDCard()) {
            PushLog.w("SD card no exist");
            return false;
        }
        String parent = FilePathUtil.getPushSdcardPath();
        String path = parent + "/" + fileName;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            String dirPath = path.substring(0, path.lastIndexOf("/"));
            File file = new File(dirPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            fos = new FileOutputStream(path);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
        } catch (Exception e) {
            PushLog.e(e);
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Get the object from file saved by method saveSerializableToSD
     * 
     * @param fileName
     *            the name of the file where get the object
     * @return serializable object
     */
    public static Serializable getSerializableBySD(String fileName) {
        if (!isExistSDCard()) {
            PushLog.w("SD card no exist");
            return null;
        }

        String path = FilePathUtil.getPushSdcardPath() + "/" + fileName;
        File sFile = new File(path);
        if (!sFile.exists()) {
            PushLog.w("File no exist");
            return null;
        }

        ObjectInputStream ois = null;
        FileInputStream fis = null;
        Serializable object = null;
        try {
            fis = new FileInputStream(sFile);
            ois = new ObjectInputStream(fis);
            object = (Serializable) ois.readObject();
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (StreamCorruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (ois != null) {
                    ois.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return object;
    }

    public static int getAppIcon(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            Intent resolveIntent = new Intent(Intent.ACTION_MAIN, null);
            resolveIntent.setPackage(pi.packageName);
            PackageManager pManager = context.getPackageManager();
            List<ResolveInfo> apps = pManager.queryIntentActivities(resolveIntent, 0);
            ResolveInfo ri = apps.iterator().next();
            if (ri != null) {
                if (ri.activityInfo.applicationInfo.icon != 0) {
                    return ri.activityInfo.applicationInfo.icon;
                }
            }
        } catch (Exception e) {
            PushLog.e(e);
        }
        return android.R.drawable.stat_sys_download_done;
    }

    public static boolean isExistSDCard() {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else
            return false;
    }

    public static byte[] getFileData(File file) {
        ByteArrayOutputStream bw = null;
        FileInputStream fs = null;
        try {
            bw = new ByteArrayOutputStream();
            fs = new FileInputStream(file);
            int count;
            byte buff[] = new byte[1024];
            while ((count = fs.read(buff, 0, 1024)) != -1) {
                bw.write(buff, 0, count);
            }
            if (bw.size() > 0) {
                return bw.toByteArray();
            }
        } catch (Exception e) {
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fs != null) {
                    fs.close();
                }
            } catch (Exception e) {
            }
        }
        return new byte[0];
    }

    public static void saveSerializableData(Context context, Serializable data, String fileName) {
        FileOutputStream fs = null;
        ObjectOutputStream os = null;

        try {
            File file = new File(FilePathUtil.getPushSdcardPath());
            if (!file.exists()) {
                file.mkdirs();
            }
            fs = new FileOutputStream(FilePathUtil.getPushSdcardPath() + fileName);
            os = new ObjectOutputStream(fs);
            os.writeObject(data);
        } catch (Exception e) {
            PushLog.e(e);
        } finally {
            try {
                if (fs != null) {
                    fs.close();
                }
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                PushLog.e(e);
            }
        }

        try {
            fs = new FileOutputStream(FilePathUtil.getPushAppPath(context) + fileName);
            os = new ObjectOutputStream(fs);
            os.writeObject(data);
        } catch (Exception e) {
            PushLog.e(e);
        } finally {
            try {
                if (fs != null) {
                    fs.close();
                }
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                PushLog.e(e);
            }
        }
    }

    public static Serializable getSerializableData(Context context, String fileName) {
        File file = new File(FilePathUtil.getPushSdcardPath() + fileName);
        Serializable data = null;
        FileInputStream fi = null;
        ObjectInputStream oi = null;

        if (!file.exists()) {
            file = new File(FilePathUtil.getPushAppPath(context) + fileName);
            if (!file.exists()) {
                return null;
            }
        }

        try {
            fi = new FileInputStream(file);
            oi = new ObjectInputStream(fi);
            data = (Serializable) oi.readObject();
        } catch (Exception e) {
            data = null;
        } finally {
            if (fi != null) {
                try {
                    fi.close();
                } catch (Exception e) {
                    PushLog.e(e);
                }
            }
            if (oi != null) {
                try {
                    oi.close();
                } catch (Exception e) {
                    PushLog.e(e);
                }
            }
        }
        return data;
    }

    public static boolean deleteFile(String path, boolean includeRoot) {
        if (path == null || path.length() == 0) {
            return false;
        }

        File file = new File(path);
        if (!file.isDirectory()) {
            file.delete();
        } else if (file.isDirectory()) {
            String[] filelist = file.list();
            if (filelist != null) {
                for (int i = 0; i < filelist.length; i++) {
                    File delfile = new File(path + "/" + filelist[i]);
                    if (!delfile.isDirectory()) {
                        delfile.delete();
                    } else if (delfile.isDirectory()) {
                        deleteFile(path + "/" + filelist[i], true);
                    }
                }
            }
            if (includeRoot) {
                file.delete();
            }
        }
        return true;
    }

    @SuppressLint("SimpleDateFormat")
    public static long getUTCTime(String strTime) {
        long utcTime = 0;

        if (strTime != null && strTime.length() != 0) {
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmm");
            try {
                Date d = df.parse(strTime);
                utcTime = d.getTime();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                PushLog.e(e);
            }
        }
        return utcTime;
    }

    /**
     * Check the app is installed or not
     * 
     * @param context
     *            App Context
     * @param packageName
     *            app package name
     * @return true Installed, false Not installed
     */
    public static boolean isAppInstalled(Context context, String packageName) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }

        return packageInfo != null;
    }

    public static int getResIdByName(Context context, String className, String name) {
        String packageName = context.getPackageName();
        Class<?> r = null;
        int id = 0;
        try {
            r = Class.forName(packageName + ".R");

            Class<?>[] classes = r.getClasses();
            Class<?> desireClass = null;

            for (int i = 0; i < classes.length; ++i) {
                if (classes[i].getName().split("\\$")[1].equals(className)) {
                    desireClass = classes[i];
                    break;
                }
            }

            if (desireClass != null) {
                id = desireClass.getField(name).getInt(desireClass);
            }
        } catch (ClassNotFoundException e) {
            PushLog.e(e);
        } catch (IllegalArgumentException e) {
            PushLog.e(e);
        } catch (SecurityException e) {
            PushLog.e(e);
        } catch (IllegalAccessException e) {
            PushLog.e(e);
        } catch (NoSuchFieldException e) {
            PushLog.e(e);
        }
        return id;
    }

    public static boolean isStringValid(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        return true;
    }

    public static Pair<String, String> getLatestServiceInfo(Context context) {
        int finalVersion = -1;
        String finalPackageName = null;
        String finalClassName = null;
        Pair<String, String> info = null;

        PackageManager pm = context.getApplicationContext().getPackageManager();
        Intent implicitIntent = new Intent(Constants.INTENT_PUSHSERVICE);
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);

        if (resolveInfos == null || resolveInfos.isEmpty()) {
            return null;
        }

        for (int i = 0; i < resolveInfos.size(); i++) {
            ResolveInfo serviceInfo = resolveInfos.get(i);
            String label = String.valueOf(serviceInfo.loadLabel(pm));
            if (label != null && label.length() > 0) {
                try {
                    int version = Integer.parseInt(label);
                    if (version > finalVersion) {
                        finalVersion = version;
                        finalPackageName = serviceInfo.serviceInfo.packageName;
                        finalClassName = serviceInfo.serviceInfo.name;
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        if (finalPackageName != null && finalClassName != null) {
            info = new Pair<String, String>(finalPackageName, finalClassName);
        }
        return info;
    }

    public static Pair<String, String> getRunningPushService(Context context) {
        Pair<String, String> destService = null;

        PackageManager pm = context.getApplicationContext().getPackageManager();
        Intent implicitIntent = new Intent(Constants.INTENT_PUSHSERVICE);
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        if (resolveInfos == null || resolveInfos.isEmpty()) {
            return null;
        }

        HashMap<String, String> pushServiceMap = new HashMap<String, String>();
        for (ResolveInfo info : resolveInfos) {
            pushServiceMap.put(info.serviceInfo.packageName, info.serviceInfo.name);
        }

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningServiceInfo> serviceList = activityManager.getRunningServices(Integer.MAX_VALUE);
        HashSet<String> allpackageSet = new HashSet<String>();
        for (RunningServiceInfo info : serviceList) {
            allpackageSet.add(info.service.getPackageName());
        }

        Collection<String> pNameSet = pushServiceMap.keySet();
        for (String pName : pNameSet) {
            if (allpackageSet.contains(pName)) {
                destService = new Pair<String, String>(pName, pushServiceMap.get(pName));
                break;
            }
        }
        return destService;
    }

    public static boolean installApkFile(Context context, File file) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            PushLog.e(e);
            return false;
        }
        return true;
    }

    /**
     * Check the current network is in WAP mode or not
     * 
     * @param context
     * @return true WAP network false not the WAP network
     */
    public static boolean isWapLink(Context context) {
        boolean isWap = false;
        String curNetworkType = DroiDataCollector.getCurNetworkType(context);
        isWap = ("cmwap".equalsIgnoreCase(curNetworkType) || "uniwap".equalsIgnoreCase(curNetworkType)
                || "ctwap".equalsIgnoreCase(curNetworkType));
        return isWap;
    }

    /**
     * Get the version name and version code for this app
     * 
     * @param context
     *            Context
     * @return An array saved the version of this app, the first element is
     *         version code while second one is version name
     */
    public static String[] getVersion(Context context) {
        String version[] = { "undef", "undef" };
        Context appContext = context.getApplicationContext();
        try {
            PackageManager manager = appContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(appContext.getPackageName(), 0);
            version[0] = Integer.toString(info.versionCode);
            version[1] = info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * Create json string which contains the details of this app
     * 
     * @param context
     *            Context
     * @param tags
     *            Tags of the app
     * @return
     */
    public static String getAppInfo(Context context, String tags) {
        String jsonStr = null;
        JSONObject obj = new JSONObject();
        String version[] = getVersion(context);
        String deviceId = DroiPushBridge.generateUUID(context.getApplicationContext());
        String channelId = getChannel(context);
        try {
            obj.put(Constants.KEY_OS_TYPE, Constants.OSTYPE);
            if (version != null && version.length == 2) {
                obj.put(Constants.KEY_APP_VERSION_CODE, version[0]);
                obj.put(Constants.KEY_APP_VERSION_NAME, version[1]);
            }
            obj.put(Constants.KEY_SDK_VERSION, Constants.SDK_VERSION);
            obj.put(Constants.KEY_DEVICE_ID, deviceId);
            obj.put(Constants.KEY_CHANNEL_ID, channelId);

            if (Util.isStringValid(tags)) {
                obj.put(Constants.KEY_APP_TAGS, tags);
            }
            jsonStr = obj.toString();
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return jsonStr;
    }

    private static String[] loadHttpServerAddress() {
        ServiceIpsBean ipBean = null;
        String[] tempUdp = null;
        String[] mServerAddress = null;
        try {
            ipBean = (ServiceIpsBean) Util.getSerializableBySD(Util.md5(Constants.DROI_SERVER_IP));
        } catch (Exception e) {
        }

        if (ipBean != null) {
            tempUdp = ipBean.getUdpAddress();
        }

        if (tempUdp != null) {
            mServerAddress = new String[tempUdp.length + 1];
            System.arraycopy(tempUdp, 0, mServerAddress, 0, tempUdp.length);
            mServerAddress[tempUdp.length] = Constants.DEFAULT_HTTP_SERVER;
        } else {
            mServerAddress = new String[1];
            mServerAddress[0] = Constants.DEFAULT_HTTP_SERVER;
        }
        return mServerAddress;
    }
    
    public static byte[] string2bytes(String ss) {
        byte[] a = null;
        if (ss != null) {
            ss = ss.replace("-", "");
            int string_len = ss.length();
            int len = string_len / 2;
            if (string_len % 2 == 1) {
                ss = "0" + ss;
                string_len++;
                len++;
            }
            a = new byte[len];
            for (int i = 0; i < len; i++) {
                a[i] = (byte) Integer.parseInt(ss.substring(2 * i, 2 * i + 2), 16);
            }
        }
        return a;
    }

    public static void sendAnalyticsInfo(AnalyticsModule analyticsModule, String header, String content) {
        if (analyticsModule != null) {
            try {
                analyticsModule.send(AnalyticsModule.TYPE_SEND_IMMEDIATELY, header, content);
            } catch (DroiException e) {
                DroiLog.e("Util", e);
            }
        }
    }

    public static String buildAnalyticsContent(String type, long msgId, int action, String deviceId) {
        String content = null;
        if (msgId < 0 || action < 0 || !Util.isStringValid(type) || !Util.isStringValid(type)) {
            return content;
        }
        long time = System.currentTimeMillis();
        try {
            JSONObject con = new JSONObject();
            con.put(KEY_ANALYTICS_MSGID, msgId);
            con.put(KEY_ANALYTICS_ACTION, action);
            con.put(KEY_ANALYTICS_TIME, time);
            con.put(KEY_ANALYTICS_DEVID, deviceId);
            JSONObject obj = new JSONObject();
            obj.put(KEY_ANALYTICS_TYPE, type);
            obj.put(KEY_ANALYTICS_CONTENT, con);
            content = obj.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return content;
    }

    public static String buildAnalyticsHeader() {
        StringBuffer buffer = new StringBuffer(Constants.ANALYTICS_TYPE_PUSH);
        buffer.append("|");
        buffer.append("1");
        buffer.append("|");
        buffer.append("-1");
        buffer.append("|");
        buffer.append("DroiPush");
        return buffer.toString();
    }
}
