package com.droi.sdk.push;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Map.Entry;
import com.droi.sdk.push.data.DatabaseColumns;
import com.droi.sdk.push.utils.Util;

import android.database.Cursor;

class PushMessageFilter {
    private long mLastReorginazeTime;
    static HashMap<String, TreeSet<PushRecord>> mHistory;

    public PushMessageFilter() {
        mLastReorginazeTime = DroiPush.mManager.getLastOrginazeTime();
        loadPushHistory();
    }

    private HashMap<String, TreeSet<PushRecord>> getPushHistory() {
        long curTime = System.currentTimeMillis();
        boolean needUpdateTime = false;
        HashMap<String, TreeSet<PushRecord>> history = new HashMap<String, TreeSet<PushRecord>>();

        Cursor c = DroiPush.mManager.getPushHistoryCursor();
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String appKey = c.getString(c.getColumnIndexOrThrow(DatabaseColumns.APPKEY));
                long msgId = c.getLong(c.getColumnIndexOrThrow(DatabaseColumns.MSG_ID));
                long createTime = c.getLong(c.getColumnIndexOrThrow(DatabaseColumns.CREATETIME));
                if (curTime - createTime >= Constants.PUSH_RECORD_KEEP_TIME_MILLISECOND) {
                    needUpdateTime = true;
                    DroiPush.mManager.deletePushRecord(appKey, msgId);
                } else {
                    TreeSet<PushRecord> set = history.get(appKey);
                    if (set == null) {
                        set = new TreeSet<PushRecord>();
                        history.put(appKey, set);
                    }
                    PushRecord record = new PushRecord(msgId, createTime);
                    set.add(record);
                }
                c.moveToNext();
            }
            c.close();
        }

        if (needUpdateTime) {
            DroiPush.mManager.saveLastReorganizeTime(curTime);
        }

        return history;
    }

    synchronized void clearOverduePushRecord() {
        long curTime = System.currentTimeMillis();
        long lastTime = mLastReorginazeTime;

        if (Math.abs(curTime - lastTime) >= Constants.PUSH_RECORD_KEEP_TIME_MILLISECOND) {
            Iterator<Entry<String, TreeSet<PushRecord>>> it1 = mHistory.entrySet().iterator();
            while (it1.hasNext()) {
                Entry<String, TreeSet<PushRecord>> entry = (Entry<String, TreeSet<PushRecord>>) it1.next();
                TreeSet<PushRecord> records = (TreeSet<PushRecord>) entry.getValue();
                String appKey = (String) entry.getKey();
                if (records != null) {
                    Iterator<PushRecord> it2 = records.iterator();
                    while (it2.hasNext()) {
                        PushRecord record = (PushRecord) it2.next();
                        if (record != null) {
                            if (Math.abs(curTime - record.create_time) >= Constants.PUSH_RECORD_KEEP_TIME_MILLISECOND) {
                                DroiPush.mManager.deletePushRecord(appKey, record.msg_id);
                                it2.remove();
                            } else {
                                break;
                            }
                        }
                    }
                    if (records.isEmpty()) {
                        it1.remove();
                    }
                }
            }
            mLastReorginazeTime = curTime;
            DroiPush.mManager.saveLastReorganizeTime(curTime);
        }
    }
    
    private void loadPushHistory(){
        mHistory = getPushHistory();
    }

    synchronized boolean isPushMessageExist(String appKey, long msgId) {
        boolean isExist = false;

        if (!Util.isStringValid(appKey) || msgId < 0) {
            return false;
        }

        if (appKey != null && appKey.length() != 0) {
            TreeSet<PushRecord> idSet = mHistory.get(appKey);
            if (idSet == null) {
                idSet = new TreeSet<PushRecord>();
                mHistory.put(appKey, idSet);
            }
            long curTime = System.currentTimeMillis();
            PushRecord record = new PushRecord(msgId, curTime);
            isExist = !(idSet.add(record));
            if (!isExist) {
                DroiPush.mManager.insertPushRecord(msgId, appKey, curTime);
            }
        }
        return isExist;
    }

    public void onDestroy(){
        if(mHistory != null){
            mHistory.clear();
        }
    }

    class PushRecord implements Comparable<PushRecord> {
        public final long msg_id;
        public final long create_time;

        public PushRecord(long id, long time) {
            msg_id = id;
            create_time = time;
        }

        @Override
        public boolean equals(Object o) {
            // TODO Auto-generated method stub
            PushRecord record = (PushRecord) o;
            if (record != null && msg_id == record.msg_id) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public int compareTo(PushRecord another) {
            // TODO Auto-generated method stub
            if (create_time > another.create_time) {
                return 1;
            } else if (create_time < another.create_time) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}