package com.droi.sdk.core;

import android.content.Context;

import com.droi.sdk.DroiException;

public class PushCoreHelper {
    /**
     * Get current application ID
     * 
     * @return Application ID
     */
    public static String getAppId() {
        return Core.getDroiAppId();
    }

    /**
     * Get device ID from CoreService
     * 
     * @param appContext
     *            Current application context
     * @return Device ID string if value isn't null
     */
    public static String getDeviceId(Context appContext) {
        String uid = null;
        try {
            Core.initialize(appContext);
        } catch (DroiException e) {
            return null;
        }
        uid = Core.getDroiDeviceId();
        return uid;
    }
}
